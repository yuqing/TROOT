#include <cmath>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <string>
#include "TLorentzVector.h"
#include "TH1F.h"
#include "TMatrixD.h"
#include <vector>
#include "TString.h"
#include "TAsy.h"

using namespace std;


class MyFunc_BackgroundFit {
public:
  MyFunc_BackgroundFit(TH1F* h): hBackground(h) {}
  Double_t operator() (Double_t *x, Double_t *p) {
    return  p[3]*hBackground->GetBinContent(hBackground->FindBin(x[0])) + p[0]*TMath::Gaus(x[0], p[1], p[2]);
  }
  TH1F* hBackground;
};


class Side_Band_Class {
public:
  Side_Band_Class(TH1F* h): sidefcn(h) {}
  Double_t operator() (Double_t *x, Double_t *p) {
    return  p[0]*sidefcn->GetBinContent(sidefcn->FindBin(x[0]));
  }
  TH1F* sidefcn;
};




TString* concat2(TString* s1, Int_t d2) {
  TString* retval = new TString(*s1);
  *retval += d2;
  return retval;
}

/////////////fit used fcns
Double_t FPOfit(Double_t *x,Double_t *par){
  ////////////////////////par[0]:Pz....par[1]:Pzs-sin....par[2]:pzc-cos....par[3]:coeff-before whole eq.
  Double_t Dilu = 0.5;
  Double_t BeamPol = 0.75;
  Double_t TarPol = 0.8;
  return  ((par[0]) + (par[1]*sin(2*x[0])) + (par[2]*cos(2*x[0])));
  //return (Dilu*BeamPol*TarPol) * ((par[0]/BeamPol) + (par[1]*sin(2*(x[0] + par[2]))));
}


Double_t FPol1(double *x, double *par) {
  return par[0]*x[0]+par[1] ;
}

/////////////class defined fcns
TAsy::TAsy(){
}

TAsy::TAsy(TDirectory* d_in// , TDirectory* d_out
	   ){
  dir_in = d_in;  
  // dir_out = d_out;
  PZ = new TGraphErrors();
}


TAsy* TAsy::Get(string target,Int_t ind,string dir, string name, Int_t topo){
  target_type = target;
  index = ind;
  inputfiledir = dir;
  inputfilename = name;
  Topo = topo;
  dir_in->cd();

  string perp = "_BPERP";
  string para = "_BPARA";
  string pos = "_TPOS";
  string neg = "_TNEG";  
  TString dir_name("psc");
  TString *n = concat2(&dir_name,index);
  string PSC(n->Data());
  ostringstream TopoDir;
  TopoDir<<"Topo"<<Topo;
  string BTb = "_BETA_b";
  string BTc = "_BETA_c";

  string MMb = "_MM_b";
  string MMc = "_MM_c";
  b_MM_BPARA_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMb+para+pos).c_str());
  b_MM_BPARA_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMb+para+neg).c_str());
  c_MM_BPARA_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMc+para+pos).c_str());
  c_MM_BPARA_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMc+para+neg).c_str());
  b_MM_BPERP_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMb+perp+pos).c_str());
  b_MM_BPERP_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMb+perp+neg).c_str());
  c_MM_BPERP_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMc+perp+pos).c_str());
  c_MM_BPERP_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+MMc+perp+neg).c_str());



  b_Beta_BPARA_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTb+para+pos).c_str());
  b_Beta_BPARA_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTb+para+neg).c_str());
  c_Beta_BPARA_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTc+para+pos).c_str());
  c_Beta_BPARA_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTc+para+neg).c_str());

  b_Beta_BPERP_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTb+perp+pos).c_str());
  b_Beta_BPERP_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTb+perp+neg).c_str());
  c_Beta_BPERP_TPOS = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTc+perp+pos).c_str());
  c_Beta_BPERP_TNEG = (TH1F*)gDirectory->Get((PSC+"_"+TopoDir.str()+BTc+perp+neg).c_str());

  entries = b_Beta_BPERP_TPOS->GetEntries() + b_Beta_BPERP_TNEG->GetEntries()
    + b_Beta_BPARA_TPOS->GetEntries() + b_Beta_BPARA_TNEG->GetEntries();
  beta_bin_nums_origin = b_Beta_BPERP_TPOS->GetXaxis()->GetNbins();

  b_Beta_BPERP_TPOS->GetXaxis()->SetTicks("+-");
  b_Beta_BPERP_TNEG->GetXaxis()->SetTicks("+-");
  b_Beta_BPARA_TPOS->GetXaxis()->SetTicks("+-");
  b_Beta_BPARA_TNEG->GetXaxis()->SetTicks("+-");  
  c_Beta_BPERP_TPOS->GetXaxis()->SetTicks("+-");
  c_Beta_BPERP_TNEG->GetXaxis()->SetTicks("+-");
  c_Beta_BPARA_TPOS->GetXaxis()->SetTicks("+-");
  c_Beta_BPARA_TNEG->GetXaxis()->SetTicks("+-");

  return this;
}


TAsy* TAsy::Get2(Double_t df, Double_t bp, Double_t tp, Bool_t ov,TBinDet* bininf// , TH1F* backa, TH1F* backb, TH1F* backc, TH1F* backd
		 ){
  DF = df;
  BEAM_POL = bp;
  TARG_POL = tp;
  coe = DF * BEAM_POL * TARG_POL;
  OL = ov;
  bt_min_2 = bt_min_1;
  bindet = bininf;
  if(OL) bt_max_2 = bt_max_1/2;
  else if(!OL) bt_max_2 = bt_max_1;


  // TH1F*bbb1 = new TH1F();
  // TH1F*bbb2 = new TH1F();
  // TH1F*bbb3 = new TH1F();
  // TH1F*bbb4 = new TH1F();
  // bbb1 = (TH1F*)backa->Clone();
  // bbb2 = (TH1F*)backb->Clone();
  // bbb3 = (TH1F*)backc->Clone();
  // bbb4 = (TH1F*)backd->Clone();
  // bbb1->Scale(0.97);
  // bbb2->Scale(1);
  // bbb3->Scale(0.97*1.15);
  // bbb4->Scale(1.15);

  // BACK = (TH1F*)bbb1->Clone();
  // BACK->Add(bbb2,1);
  // BACK->Add(bbb3,1);
  // BACK->Add(bbb4,1);

  // BACK = backa;
  return this;
}



TAsy* TAsy::GetNormFactors(Double_t a, Double_t b, Double_t c, Double_t d){
  norm_para_pos = a;
  norm_para_neg = b;
  norm_perp_pos = c;
  norm_perp_neg = d;
  return this;
}


// TAsy* TAsy::GetNormFactor(){

//   Double_t entry_perp_pos_c = MM_c_BPERP_TPOS->GetEntries();
//   Double_t entry_perp_neg_c = MM_c_BPERP_TNEG->GetEntries();
//   Double_t entry_para_pos_c = MM_c_BPARA_TPOS->GetEntries();
//   Double_t entry_para_neg_c = MM_c_BPARA_TNEG->GetEntries();
//   Double_t entry_perp_pos_CH = MM_CH_BPERP_TPOS->GetEntries();
//   Double_t entry_perp_neg_CH = MM_CH_BPERP_TNEG->GetEntries();
//   Double_t entry_para_pos_CH = MM_CH_BPARA_TPOS->GetEntries();
//   Double_t entry_para_neg_CH = MM_CH_BPARA_TNEG->GetEntries();

//   // cout<<entry_perp_pos_c<<"   "<<entry_perp_pos_CH<<"   "<<entry_perp_pos_c/entry_perp_pos_CH<<endl
//   //     <<entry_perp_neg_c<<"   "<<entry_perp_neg_CH<<"   "<<entry_perp_neg_c/entry_perp_neg_CH<<endl
//   //     <<entry_para_pos_c<<"   "<<entry_para_pos_CH<<"   "<<entry_para_pos_c/entry_para_pos_CH<<endl
//   //     <<entry_para_neg_c<<"   "<<entry_para_neg_CH<<"   "<<entry_para_neg_c/entry_para_neg_CH<<endl;

//   entry_perp_pos = entry_perp_pos_c + entry_perp_pos_CH;
//   entry_perp_neg = entry_perp_neg_c + entry_perp_neg_CH;
//   entry_para_pos = entry_para_pos_c + entry_para_pos_CH;
//   entry_para_neg = entry_para_neg_c + entry_para_neg_CH;


//   norm_perp = (entry_perp_pos) / (entry_perp_neg);
//   norm_para = (entry_para_pos) / (entry_para_neg);

//   // cout<<norm_perp<<"   "<<norm_para<<endl;
//   return this;
// }




// TH1F* TAsy::Boundary(string beam_pol, TH1F* h1){
//   TH1F* H1 = new TH1F();
//   H1 = (TH1F*)h1->Clone("H1");
//   ifstream cutpara;
//   Int_t cutpoints;
//   if(OL){
//     cutpara.open("parameter_input_p2t1_overlap.txt");
//     cutpoints = 6;
//   }
//   else if (!OL){
//     cutpara.open("parameter_input_p2t1.txt"); 
//     cutpoints = 12;
//   }
//   string content;
//   while(cutpara>>content){
//     if(content=="perp:"){
//       cutranges_perp.clear();
//       cut_bin_perp.clear();
//       for(Int_t i=0;i<cutpoints;i++){
// 	Double_t value;
// 	cutpara>>value;
// 	cutranges_perp.push_back(value);
// 	for(Int_t j=1; j<=H1->GetXaxis()->GetNbins(); j++){
// 	  if(value>H1->GetBinLowEdge(j) 
// 	     && value<=H1->GetBinLowEdge(j+1)){
// 	    cut_bin_perp.push_back(j);
// 	    // cout<<j<<endl;
// 	  }
// 	}
//       }
//     }
//     else if(content=="para:"){
//       cutranges_para.clear();
//       cut_bin_para.clear();
//       //cout<<endl;
//       for(Int_t i=0;i<cutpoints;i++){
// 	Double_t value;
// 	cutpara>>value;
// 	cutranges_para.push_back(value);
// 	for(Int_t j=1; j<=H1->GetXaxis()->GetNbins(); j++){
// 	  if(value>H1->GetBinLowEdge(j) 
// 	     && value<=H1->GetBinLowEdge(j+1)){
// 	    cut_bin_para.push_back(j);
// 	    //cout<<j<<endl;
// 	  }
// 	}
//       }
//     }
//   }
//   if(beam_pol == "PERP"){
//     for(Int_t i=0; i<cutpoints/2; i++){
//       for(Int_t j=1; j<cut_bin_perp[i*2+1]-cut_bin_perp[i*2]; j++){
//   	H1->SetBinContent(cut_bin_perp[i*2]+j,0);
//   	H1->SetBinError(cut_bin_perp[i*2]+j,0);
//       }
//     }
//     // cout<<endl<<endl<<"part  1  :"<<cut_bin_perp[0]<<endl;
//     // for(Int_t k=0; k<5; k++){
//     //   cout<<"part  "<<k+2<<"  :"<<cut_bin_perp[k*2+2]-cut_bin_perp[k*2+1]+1<<endl;
//     // }
//     // cout<<"part  7  :"<<H1->GetXaxis()->GetNbins()-cut_bin_perp[11]+1;
//   }
//   else if(beam_pol == "PARA"){
//     for(Int_t i=1; i<=(cutpoints/2-1); i++){
//       for(Int_t j=1; j<cut_bin_para[i*2]-cut_bin_para[i*2-1]; j++){
//   	H1->SetBinContent(cut_bin_para[i*2-1]+j,0);
//   	H1->SetBinError(cut_bin_para[i*2-1]+j,0);
//       }
//     }
//     for(Int_t j=1; j<cut_bin_para[0]; j++){
//       H1->SetBinContent(j,0);
//       H1->SetBinError(j,0);
//     }
//     for(Int_t j=1; j<H1->GetXaxis()->GetNbins()-cut_bin_para[cut_bin_para.size()-1]; j++){
//       H1->SetBinContent(cut_bin_para[cut_bin_para.size()-1]+j,0);
//       H1->SetBinError(cut_bin_para[cut_bin_para.size()-1]+j,0);
//     }
//   }
//   cutpara.close();
//   H1->SetNameTitle(beam_pol.c_str(),beam_pol.c_str());
//   return H1;
// }



TH1F* TAsy::Overlap(string beam_pol, string tar_pol, TH1F* h1){
  TH1F* H1 = new TH1F();
  H1 = (TH1F*)h1->Clone("H1");
  Int_t ini_num_bin = H1->GetNbinsX();
  Int_t fin_num_bin = ini_num_bin/2;

  Double_t fin_lows[fin_num_bin+1];
  for(Int_t i=1; i<=fin_num_bin+1; i++) fin_lows[i-1] = H1->GetXaxis()->GetBinLowEdge(i);

  TH1F* H2 = new TH1F((beam_pol+"_"+tar_pol).c_str(), (beam_pol+"_"+tar_pol).c_str(),fin_num_bin,fin_lows);
  Double_t bincontent;
  Double_t binerror;
  for(Int_t i=1; i<=fin_num_bin; i++){
    bincontent = Double_t(H1->GetBinContent(i)) + Double_t(H1->GetBinContent(i+fin_num_bin));
    binerror = sqrt(Double_t(H1->GetBinError(i))*Double_t(H1->GetBinError(i)) 
		    + Double_t(H1->GetBinError(i+fin_num_bin))*Double_t(H1->GetBinError(i+fin_num_bin)));
    H2->SetBinContent(i, bincontent);
    H2->SetBinError(i, binerror);
    // cout<<i<<"   "<< H2->GetXaxis()->GetBinLowEdge(i)<<"   "<<bincontent<<"    "<<binerror<<endl;
  }

  delete H1;
  return H2;
}



TH1F* TAsy::Flip_Ninty_Deg(TH1F* h1){
  TH1F* H1 = new TH1F();
  H1 = (TH1F*)h1->Clone("H1");
  Int_t ini_num_bin = H1->GetNbinsX();
  Int_t half = ini_num_bin/2;
  Int_t quar1 = ini_num_bin*1/4;
  Int_t quar3 = ini_num_bin*3/4;

  Double_t lows[ini_num_bin+1];
  for(Int_t i=1; i<=ini_num_bin+1; i++) lows[i-1] = H1->GetXaxis()->GetBinLowEdge(i);

  TH1F* H2 = new TH1F("H2", "H2",ini_num_bin,lows);
  for(Int_t i=1; i<=ini_num_bin; i++){
    Double_t bincontent;
    Double_t binerror;
    // if(i<=half){
    //   bincontent = Double_t(H1->GetBinContent(i+half));
    //   binerror   = Double_t(H1->GetBinError(i+half));
    //   H2->SetBinContent(i, bincontent);
    //   H2->SetBinError(i, binerror);
    // }
    // else if(i>half){
    //   bincontent = Double_t(H1->GetBinContent(i-half));
    //   binerror   = Double_t(H1->GetBinError(i-half));
    //   H2->SetBinContent(i, bincontent);
    //   H2->SetBinError(i, binerror);
    // }
    if(i<=quar3){
      bincontent = Double_t(H1->GetBinContent(i));
      binerror   = Double_t(H1->GetBinError(i));
      H2->SetBinContent(i+quar1, bincontent);
      H2->SetBinError(i+quar1, binerror);
    }
    else if(i>quar3){
      bincontent = Double_t(H1->GetBinContent(i));
      binerror   = Double_t(H1->GetBinError(i));
      H2->SetBinContent(i-quar3, bincontent);
      H2->SetBinError(i-quar3, binerror);
    }
    // cout<<i<<"   "<< H2->GetXaxis()->GetBinLowEdge(i)<<"   "<<bincontent<<"    "<<binerror<<endl;
  }

  delete H1;
  return H2;
}



TH1F* TAsy::Flip_Phi(TH1F* h1, Double_t angle){
  TH1F* H1 = new TH1F();
  H1 = (TH1F*)h1->Clone("H1");
  Int_t ini_num_bin = H1->GetNbinsX();
  Int_t half = ini_num_bin/2;
  Int_t quar1 = ini_num_bin*1/4;
  Int_t quar3 = ini_num_bin*3/4;

  Double_t lows[ini_num_bin+1];
  for(Int_t i=1; i<=ini_num_bin+1; i++) lows[i-1] = H1->GetXaxis()->GetBinLowEdge(i);

  TH1F* H2 = new TH1F("H2", "H2",ini_num_bin,lows);
  for(Int_t i=1; i<=ini_num_bin; i++){
    Double_t bincontent;
    Double_t binerror;
    if(i<=half){
      bincontent = Double_t(H1->GetBinContent(i+half));
      binerror   = Double_t(H1->GetBinError(i+half));
      H2->SetBinContent(i, bincontent);
      H2->SetBinError(i, binerror);
    }
    else if(i>half){
      bincontent = Double_t(H1->GetBinContent(i-half));
      binerror   = Double_t(H1->GetBinError(i-half));
      H2->SetBinContent(i, bincontent);
      H2->SetBinError(i, binerror);
    }
    // if(i<=quar3){
    //   bincontent = Double_t(H1->GetBinContent(i));
    //   binerror   = Double_t(H1->GetBinError(i));
    //   H2->SetBinContent(i+quar1, bincontent);
    //   H2->SetBinError(i+quar1, binerror);
    // }
    // else if(i>quar3){
    //   bincontent = Double_t(H1->GetBinContent(i));
    //   binerror   = Double_t(H1->GetBinError(i));
    //   H2->SetBinContent(i-quar3, bincontent);
    //   H2->SetBinError(i-quar3, binerror);
    // }
    // cout<<i<<"   "<< H2->GetXaxis()->GetBinLowEdge(i)<<"   "<<bincontent<<"    "<<binerror<<endl;
  }

  delete H1;
  return H2;
}




vector<TH1F*> TAsy::Part(string beam_pol, string tar_pol, TH1F* h1){
  TH1F* H1 = new TH1F();
  H1 = (TH1F*)h1->Clone("H1");
  Int_t ini_num_bin = H1->GetNbinsX();
  Int_t fin_num_bin = ini_num_bin/2;

  Double_t fin_lows[fin_num_bin+1];
  for(Int_t i=1; i<=fin_num_bin+1; i++) fin_lows[i-1] = H1->GetXaxis()->GetBinLowEdge(i);

  TH1F* Hf = new TH1F((beam_pol+"_"+tar_pol+"front").c_str(), (beam_pol+"_"+tar_pol).c_str(),fin_num_bin,fin_lows);
  TH1F* Hb = new TH1F((beam_pol+"_"+tar_pol+"back").c_str(), (beam_pol+"_"+tar_pol).c_str(),fin_num_bin,fin_lows);

  for(Int_t i=1; i<=fin_num_bin; i++){
    Double_t bincontent_f;
    Double_t bincontent_b;  
    Double_t binerror_f;
    Double_t binerror_b;
    bincontent_f = Double_t(H1->GetBinContent(i));
    bincontent_b = Double_t(H1->GetBinContent(i+fin_num_bin));
    binerror_f = Double_t(H1->GetBinError(i));
    binerror_b = Double_t(H1->GetBinError(i+fin_num_bin));
    Hf->SetBinContent(i, bincontent_f);
    Hf->SetBinError(i, binerror_f);
    Hb->SetBinContent(i, bincontent_b);
    Hb->SetBinError(i, binerror_b);
  }

  vector<TH1F*> part;
  part.push_back(Hf);
  part.push_back(Hb);

  delete H1;
  return part;
}



// TH1F* TAsy::DynamicRebin(string beam_pol, string tar_pol, TH1F* x){
//   Int_t rebin;
//   TH1F* input = new TH1F();
//   input = (TH1F*)x->Clone();
//   vector<Int_t> binset_perp;
//   vector<Int_t> binset_para; 
//   ifstream cutpara; 
//   Int_t bin_select_loops;
//   if(OL){
//     cutpara.open("parameter_input_p2t1_overlap.txt");
//     bin_select_loops = 4;
//   }
//   else if (!OL){
//     cutpara.open("parameter_input_p2t1.txt"); 
//     bin_select_loops = 7;
//   }
//   string content;
//   Int_t binnum_perp(0);
//   Int_t binnum_para(0);

//   while(cutpara>>content){
//     if(target_type == "butanol" || (target_type == "carbon") || (target_type == "CH2")){
//       if(content=="perp_bin:"){
// 	for(Int_t i=0;i<bin_select_loops;i++){
// 	  Int_t value;
// 	  cutpara>>value;
// 	  binset_perp.push_back(value);
// 	  binnum_perp += value;
// 	  if(i!=(bin_select_loops-1)){
// 	    excluded_bin_perp.push_back(binnum_perp+i+1);
// 	  }
// 	}
//       }
//       else if(content=="para_bin:"){
// 	for(Int_t i=0;i<bin_select_loops;i++){
// 	  excluded_bin_para.push_back(binnum_para+i+1);
// 	  if(i!=(bin_select_loops-1)){
// 	    Int_t value;
// 	    cutpara>>value;
// 	    binset_para.push_back(value);
// 	    binnum_para += value;
// 	  }

// 	}
//       }
//     }
//   }
//   binnum_perp += (bin_select_loops-1);
//   binnum_para += bin_select_loops;

//   if(beam_pol == "PERP"){
//     vector<Int_t> blocksizes;
//     Int_t b0 = cut_bin_perp[0]/binset_perp[0];
//     Int_t b1 = (cut_bin_perp[2]-cut_bin_perp[1]+1)/binset_perp[1];
//     Int_t b2 = (cut_bin_perp[4]-cut_bin_perp[3]+1)/binset_perp[2];
//     for(Int_t l=0;l<binset_perp[0];l++)blocksizes.push_back(b0);
//     for(Int_t l=0;l<binset_perp[1];l++)blocksizes.push_back(b1);
//     for(Int_t l=0;l<binset_perp[2];l++)blocksizes.push_back(b2);

//     if(!OL){
//       Int_t b3 = (cut_bin_perp[6]-cut_bin_perp[5]+1)/binset_perp[3];
//       Int_t b4 = (cut_bin_perp[8]-cut_bin_perp[7]+1)/binset_perp[4];
//       Int_t b5 = (cut_bin_perp[10]-cut_bin_perp[9]+1)/binset_perp[5];
//       for(Int_t l=0;l<binset_perp[3];l++)blocksizes.push_back(b3);
//       for(Int_t l=0;l<binset_perp[4];l++)blocksizes.push_back(b4);
//       for(Int_t l=0;l<binset_perp[5];l++)blocksizes.push_back(b5);
//     }

//     Int_t b6 = (input->GetXaxis()->GetNbins()-cut_bin_perp[cut_bin_perp.size()-1]+1)/binset_perp[bin_select_loops-1];
//     for(Int_t l=0;l<binset_perp[bin_select_loops-1];l++)blocksizes.push_back(b6);


//     vector<Int_t> startbins;
//     startbins.push_back(1);
//     startbins.push_back(1+blocksizes[0]);
//     for(Int_t k=0;k<(bin_select_loops-1);k++){
//       for(Int_t l=0;l<binset_perp[k+1];l++){
// 	startbins.push_back(cut_bin_perp[1+k*2]+blocksizes[2+k*3]*l);
//       }
//     }
//     vector<Int_t> lowedgebins;
//     vector<Double_t> lowedgevalues;
//     Double_t Lowedgevalues[binnum_perp+1];///should be 25 contents
//     lowedgebins.push_back(1);
//     lowedgebins.push_back(1+blocksizes[0]);
//     lowedgebins.push_back(1+blocksizes[0]*2);
//     lowedgevalues.push_back(1*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//     lowedgevalues.push_back((1+blocksizes[0])*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//     lowedgevalues.push_back((1+blocksizes[0]*2)*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//     for(Int_t k=0;k<(bin_select_loops-1);k++){
//       for(Int_t l=0;l<binset_perp[k+1]+1;l++){
// 	lowedgebins.push_back(cut_bin_perp[1+k*2]+blocksizes[2+k*3]*l);
// 	lowedgevalues.push_back((cut_bin_perp[1+k*2]+blocksizes[2+k*3]*l)
// 				*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//       }
//     }
//     for(vector<Int_t>::size_type i=0; i<lowedgevalues.size(); i++){
//       Lowedgevalues[i] = lowedgevalues[i];
//     }
//     // cout<<startbins.size()<<endl;
//     //  for(vector<Int_t>::size_type i=0; i<startbins.size(); i++){
//     //    cout<<endl<<endl<<startbins[i]<<"    "<< startbins[i]+blocksizes[i]-1<<endl;
//     //  }
//     // for(vector<Int_t>::size_type i=0; i<lowedgebins.size(); i++){
//     //   cout<<endl<<i+1<<"   "<<lowedgebins[i]<<endl;
//     // }
//     // cout<<lowedgevalues.size()<<endl;
//     vector<Double_t> Content;
//     vector<Double_t> Error;
//     for(vector<Int_t>::size_type i=0; i<lowedgebins.size()-1; i++){
//       Double_t content(0.);
//       Double_t error(0.);
//       for(Int_t j=0; j<lowedgebins[i+1]-lowedgebins[i]; j++){
//     	content += input->GetBinContent(lowedgebins[i]+j);
//     	error += input->GetBinError(lowedgebins[i]+j)*input->GetBinError(lowedgebins[i]+j);
//       }
//       error = sqrt(error);
//       Content.push_back(content);
//       Error.push_back(error);
//     }

//     /////////////////////////
//     TH1F* output = new TH1F("output","output",Content.size(),bt_min_2,bt_max_2);
//     output->GetXaxis()->Set(binnum_perp,Lowedgevalues);
//     output->SetNameTitle((beam_pol+"_"+tar_pol).c_str(), (beam_pol+tar_pol).c_str());
//     ofstream x;
//     x.open("testcontent.txt",ios::app);
//     x<<endl<<endl;
//     for(vector<Double_t>::size_type i=0; i<Content.size(); i++){
//       output->SetBinContent(i+1,Content[i]);
//       output->SetBinError(i+1,Error[i]);
//       x<<Content[i]<<endl;
//     }
//     delete input;
//     return output;
//   }


//   else if(beam_pol == "PARA"){
//     vector<Int_t> blocksizes;
//     Int_t b0 = (cut_bin_para[1]-cut_bin_para[0]+1)/binset_para[0];
//     Int_t b1 = (cut_bin_para[3]-cut_bin_para[2]+1)/binset_para[1];
//     Int_t b2 = (cut_bin_para[5]-cut_bin_para[4]+1)/binset_para[2];
//     for(Int_t l=0;l<binset_para[0];l++)blocksizes.push_back(b0);
//     for(Int_t l=0;l<binset_para[1];l++)blocksizes.push_back(b1);
//     for(Int_t l=0;l<binset_para[2];l++)blocksizes.push_back(b2);
//     if(!OL){
//       Int_t b3 = (cut_bin_para[7]-cut_bin_para[6]+1)/binset_para[3];
//       Int_t b4 = (cut_bin_para[9]-cut_bin_para[8]+1)/binset_para[4];
//       Int_t b5 = (cut_bin_para[11]-cut_bin_para[10]+1)/binset_para[5];
//       for(Int_t l=0;l<binset_para[3];l++)blocksizes.push_back(b3);
//       for(Int_t l=0;l<binset_para[4];l++)blocksizes.push_back(b4);
//       for(Int_t l=0;l<binset_para[5];l++)blocksizes.push_back(b5);
//     }
   
//     vector<Int_t> lowedgebins;
//     vector<Double_t> lowedgevalues;
//     Double_t Lowedgevalues[binnum_para+1];///should be 25 contents
//     lowedgebins.push_back(1);
//     lowedgevalues.push_back(1*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//     for(Int_t k=0;k<(bin_select_loops-1);k++){
//       for(Int_t l=0;l<binset_para[k]+1;l++){
// 	lowedgebins.push_back(cut_bin_para[k*2]+blocksizes[k*3]*l);
// 	lowedgevalues.push_back((cut_bin_para[k*2]+blocksizes[k*3]*l)
// 				*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//       }
//     }
//     lowedgebins.push_back(input->GetXaxis()->GetNbins());
//     lowedgevalues.push_back(input->GetXaxis()->GetNbins()*2*pai_TAsy/(input->GetXaxis()->GetNbins()));
//     for(vector<Int_t>::size_type i=0; i<lowedgevalues.size(); i++){
//       Lowedgevalues[i] = lowedgevalues[i];
//     }
//     // cout<<startbins.size()<<endl;
//     //  for(vector<Int_t>::size_type i=0; i<startbins.size(); i++){
//     //    cout<<endl<<endl<<startbins[i]<<"    "<< startbins[i]+blocksizes[i]-1<<endl;
//     //  }
//     // for(vector<Int_t>::size_type i=0; i<lowedgebins.size(); i++){
//     //   cout<<endl<<i+1<<"   "<<lowedgebins[i]<<endl;
//     //  }
//     // cout<<lowedgevalues.size()<<endl;
//     vector<Double_t> Content;
//     vector<Double_t> Error;
//     for(vector<Int_t>::size_type i=0; i<lowedgebins.size()-1; i++){
//       Double_t content(0.);
//       Double_t error(0.);
//       for(Int_t j=0; j<lowedgebins[i+1]-lowedgebins[i]; j++){
//     	content += input->GetBinContent(lowedgebins[i]+j);
//     	error += input->GetBinError(lowedgebins[i]+j)*input->GetBinError(lowedgebins[i]+j);
//       }
//       error = sqrt(error);
//       Content.push_back(content);
//       Error.push_back(error);
//     }

//     /////////////////////////
//     TH1F* output = new TH1F("output","output",Content.size(),bt_min_2,bt_max_2);
//     output->GetXaxis()->Set(binnum_para,Lowedgevalues);
//     output->SetNameTitle((beam_pol+"_"+tar_pol).c_str(), (beam_pol+tar_pol).c_str());
//     ofstream x;
//     x.open("testcontent.txt",ios::app);
//     x<<endl<<endl;
//     for(vector<Double_t>::size_type i=0; i<Content.size(); i++){
//       output->SetBinContent(i+1,Content[i]);
//       output->SetBinError(i+1,Error[i]);
//       x<<Content[i]<<endl;
//     }
//     delete input;
//     return output;
//   }
// }















// TH1F* TAsy::DynamicRebin(TH1F* x, Int_t num){
//   TH1F* input = new TH1F();
//   input = (TH1F*)x->Clone();



//   Double_t cut1 = 0.13;
//   ////fill this
//   Double_t cut2 = 0.94;
//   ////
//   Double_t cut3 = 1.20;
//   ////fill this
//   Double_t cut4 = 2.00;
//   ////
//   Double_t cut5 = 2.26;
//   ////fill this
//   Double_t cut6 = 3.05;



//   Int_t num_bins = 4+num*3;
//   Double_t lowedge[num_bins+1];

//   lowedge[0] = 0.;
//   lowedge[1] = cut1;

//   for(Int_t B1=0;B1<num;B1++){lowedge[B1+2] = cut1+(B1+1)*(cut2-cut1)/num;}
//   lowedge[2+num] = cut3;
//   for(Int_t B2=0;B2<num;B2++){lowedge[B2+3+num] = cut3+(B2+1)*(cut4-cut3)/num;}
//   lowedge[1+2*(num+1)] = cut5;
//   for(Int_t B3=0;B3<num;B3++){lowedge[1+2*(num+1)+1+B3] = cut5+(B3+1)*(cut6-cut5)/num;}
//   lowedge[num_bins] = 3.1416926;

//   /////set bin content
//   TH1F *ret = new TH1F(x->GetName(),x->GetName(),num_bins,lowedge);

//   //_______________
//   ret->SetBinContent(1, 0.);
//   ret->SetBinError(1, 0.);
//   for(Int_t B1=2;B1<2+num;B1++){
//     Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
//     ret->SetBinContent(B1, thiscontent);
//     ret->SetBinError(B1, sqrt(thiscontent));
//   }
//   //_______________
//   ret->SetBinContent(2+num, 0.);
//   ret->SetBinError(2+num, 0.);
//   for(Int_t B1=2+num+1;B1<2+num+1+num;B1++){
//     Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
//     ret->SetBinContent(B1, thiscontent);
//     ret->SetBinError(B1, sqrt(thiscontent));
//   }
//   //_______________
//   ret->SetBinContent(1+2*(num+1), 0.);
//   ret->SetBinError(1+2*(num+1), 0.);
//   for(Int_t B1=1+2*(num+1)+1;B1<1+2*(num+1)+1+num;B1++){
//     Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
//     ret->SetBinContent(B1, thiscontent);
//     ret->SetBinError(B1, sqrt(thiscontent));
//   }
//   //_______________
//   ret->SetBinContent(num_bins, 0.);
//   ret->SetBinError(num_bins, 0.);

//   delete input;
//   return ret;
  
// }










TH1F* TAsy::DynamicRebin(TH1F* x, Int_t num, Double_t fiducialcut){
  TH1F* input = new TH1F();
  input = (TH1F*)x->Clone();

  Double_t fid = fiducialcut*3.1415926/180.;

  Double_t cut1 = 0.13+fid;
  ////fill this
  Double_t cut2 = 0.94-fid;
  ////
  Double_t cut3 = 1.18+fid;
  ////fill this
  Double_t cut4 = 1.99-fid;
  ////
  Double_t cut5 = 2.23+fid;
  ////fill this
  Double_t cut6 = 3.04-fid;
  ////
  Double_t cut7 = 3.27+fid;
  ////fill this
  Double_t cut8 = 4.08-fid;
  ////
  Double_t cut9 = 4.32+fid;
  ////fill this
  Double_t cut10 = 5.13-fid;
  ////
  Double_t cut11 = 5.36+fid;
  ////fill this
  Double_t cut12 = 6.18-fid;



  Int_t num_bins = 7+num*6;
  Double_t lowedge[num_bins+1];

  lowedge[0] = 0.;
  lowedge[1] = cut1;
  for(Int_t B1=0;B1<num;B1++){lowedge[B1+2] = cut1+(B1+1)*(cut2-cut1)/num;}

  lowedge[2+num] = cut3;
  for(Int_t B2=0;B2<num;B2++){lowedge[B2+3+num] = cut3+(B2+1)*(cut4-cut3)/num;}

  lowedge[1+2*(num+1)] = cut5;
  for(Int_t B3=0;B3<num;B3++){lowedge[1+2*(num+1)+1+B3] = cut5+(B3+1)*(cut6-cut5)/num;}

  lowedge[1+3*(num+1)] = cut7;
  for(Int_t B4=0;B4<num;B4++){lowedge[1+3*(num+1)+1+B4] = cut7+(B4+1)*(cut8-cut7)/num;}

  lowedge[1+4*(num+1)] = cut9;
  for(Int_t B4=0;B4<num;B4++){lowedge[1+4*(num+1)+1+B4] = cut9+(B4+1)*(cut10-cut9)/num;}

  lowedge[1+5*(num+1)] = cut11;
  for(Int_t B4=0;B4<num;B4++){lowedge[1+5*(num+1)+1+B4] = cut11+(B4+1)*(cut12-cut11)/num;}

  lowedge[num_bins] = 3.1416926*2;



  /////set bin content
  TH1F *ret = new TH1F(x->GetName(),x->GetName(),num_bins,lowedge);

  //_______________
  ret->SetBinContent(1, 0.);
  ret->SetBinError(1, 0.);
  for(Int_t B1=2;B1<2+num;B1++){
    Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
    ret->SetBinContent(B1, thiscontent);
    ret->SetBinError(B1, sqrt(thiscontent));
  }
  //_______________
  ret->SetBinContent(2+num, 0.);
  ret->SetBinError(2+num, 0.);
  for(Int_t B1=2+num+1;B1<2+num+1+num;B1++){
    Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
    ret->SetBinContent(B1, thiscontent);
    ret->SetBinError(B1, sqrt(thiscontent));
  }
  //_______________
  ret->SetBinContent(1+2*(num+1), 0.);
  ret->SetBinError(1+2*(num+1), 0.);
  for(Int_t B1=1+2*(num+1)+1;B1<1+2*(num+1)+1+num;B1++){
    Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
    ret->SetBinContent(B1, thiscontent);
    ret->SetBinError(B1, sqrt(thiscontent));
  }
  //_______________
  ret->SetBinContent(1+3*(num+1), 0.);
  ret->SetBinError(1+3*(num+1), 0.);
  for(Int_t B1=1+3*(num+1)+1;B1<1+3*(num+1)+1+num;B1++){
    Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
    ret->SetBinContent(B1, thiscontent);
    ret->SetBinError(B1, sqrt(thiscontent));
  }
  //_______________
  ret->SetBinContent(1+4*(num+1), 0.);
  ret->SetBinError(1+4*(num+1), 0.);
  for(Int_t B1=1+4*(num+1)+1;B1<1+4*(num+1)+1+num;B1++){
    Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
    ret->SetBinContent(B1, thiscontent);
    ret->SetBinError(B1, sqrt(thiscontent));
  }
  //_______________
  ret->SetBinContent(1+5*(num+1), 0.);
  ret->SetBinError(1+5*(num+1), 0.);
  for(Int_t B1=1+5*(num+1)+1;B1<1+5*(num+1)+1+num;B1++){
    Double_t thiscontent = input->Integral(input->FindBin(lowedge[B1-1]), input->FindBin(lowedge[B1]));
    ret->SetBinContent(B1, thiscontent);
    ret->SetBinError(B1, sqrt(thiscontent));
  }
  //_______________
  ret->SetBinContent(num_bins, 0.);
  ret->SetBinError(num_bins, 0.);

  delete input;
  return ret;
  
}





TH1F* TAsy::DynamicNorm(TH1F* x, Double_t norm_fac){
  // set bin content
  Int_t num_bins = x->GetXaxis()->GetNbins();
  Double_t lowedge[num_bins+1];
  x->GetLowEdge(lowedge);
  TH1F *ret = new TH1F(x->GetName(),x->GetName(),x->GetXaxis()->GetNbins(),lowedge);

  for(Int_t i=1; i<=num_bins; i++){
    ret->SetBinContent(i, x->GetBinContent(i)*norm_fac);
    // if(norm_fac>1)
    ret->SetBinError(i, (x->GetBinError(i)*1.)*norm_fac);
    // else if(norm_fac<1)
    //  ret->SetBinError(i, sqrt(x->GetBinContent(i)));
  }
  return ret;
  
}



TH1F* TAsy::phizero(TH1F* x, Double_t phase){

  Int_t num_bins = x->GetXaxis()->GetNbins();
  Double_t lowedge[num_bins+1];
  Double_t newlowedge[num_bins+1];
  x->GetLowEdge(lowedge);
  newlowedge[0] = lowedge[0];
  newlowedge[num_bins] = lowedge[num_bins];

  for(Int_t i=1; i<num_bins; i++){
    newlowedge[i] = lowedge[i]+phase*3.1415926/180;
  }
  TH1F *ret = new TH1F(x->GetName(),x->GetName(),x->GetXaxis()->GetNbins(),newlowedge);

  for(Int_t i=1; i<=num_bins; i++){
    ret->SetBinContent(i, x->GetBinContent(i));
    ret->SetBinError(i, x->GetBinError(i));
  }
  return ret;
}







vector<TGraph*> TAsy::BetaStats(TH1F* b_BPERP_TPOS, TH1F* b_BPERP_TNEG,
				TH1F* b_BPARA_TPOS, TH1F* b_BPARA_TNEG,
				TH1F* c_BPERP_TPOS, TH1F* c_BPERP_TNEG,
				TH1F* c_BPARA_TPOS, TH1F* c_BPARA_TNEG){

  Int_t binnum = b_BPERP_TPOS->GetXaxis()->GetNbins();
  Double_t x[binnum], yb[binnum], yc[binnum];
  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t B1 = b_BPERP_TPOS->GetBinContent(i);
    Double_t B2 = b_BPERP_TNEG->GetBinContent(i); 
    Double_t B3 = b_BPARA_TPOS->GetBinContent(i);
    Double_t B4 = b_BPARA_TNEG->GetBinContent(i);
    Double_t C1 = c_BPERP_TPOS->GetBinContent(i);
    Double_t C2 = c_BPERP_TNEG->GetBinContent(i); 
    Double_t C3 = c_BPARA_TPOS->GetBinContent(i);
    Double_t C4 = c_BPARA_TNEG->GetBinContent(i);
    x[i] = b_BPERP_TPOS->GetBinCenter(i);
    yb[i] = B1+B2+B3+B4;
    yc[i] = C1+C2+C3+C4;
  }
  TGraph *GB = new TGraph (binnum, x, yb);
  TGraph *GC = new TGraph (binnum, x, yc);
  vector<TGraph*> bt;
  bt.push_back(GB);
  bt.push_back(GC);
  return bt;
}



TAsy* TAsy::Rebin(Int_t rebin, Double_t fiducialcut){
  b_Beta_BPERP_TPOS_front = (TAsy::Part("PERP", "POS", b_Beta_BPERP_TPOS)).at(0);
  b_Beta_BPERP_TNEG_front = (TAsy::Part("PERP", "NEG", b_Beta_BPERP_TNEG)).at(0);
  b_Beta_BPARA_TPOS_front = (TAsy::Part("PARA", "POS", b_Beta_BPARA_TPOS)).at(0);
  b_Beta_BPARA_TNEG_front = (TAsy::Part("PARA", "NEG", b_Beta_BPARA_TNEG)).at(0);
  c_Beta_BPERP_TPOS_front = (TAsy::Part("PERP", "POS", c_Beta_BPERP_TPOS)).at(0);
  c_Beta_BPERP_TNEG_front = (TAsy::Part("PERP", "NEG", c_Beta_BPERP_TNEG)).at(0);
  c_Beta_BPARA_TPOS_front = (TAsy::Part("PARA", "POS", c_Beta_BPARA_TPOS)).at(0);
  c_Beta_BPARA_TNEG_front = (TAsy::Part("PARA", "NEG", c_Beta_BPARA_TNEG)).at(0);

  b_Beta_BPERP_TPOS_back = (TAsy::Part("PERP", "POS", b_Beta_BPERP_TPOS)).at(1);
  b_Beta_BPERP_TNEG_back = (TAsy::Part("PERP", "NEG", b_Beta_BPERP_TNEG)).at(1);
  b_Beta_BPARA_TPOS_back = (TAsy::Part("PARA", "POS", b_Beta_BPARA_TPOS)).at(1);
  b_Beta_BPARA_TNEG_back = (TAsy::Part("PARA", "NEG", b_Beta_BPARA_TNEG)).at(1);
  c_Beta_BPERP_TPOS_back = (TAsy::Part("PERP", "POS", c_Beta_BPERP_TPOS)).at(1);
  c_Beta_BPERP_TNEG_back = (TAsy::Part("PERP", "NEG", c_Beta_BPERP_TNEG)).at(1);
  c_Beta_BPARA_TPOS_back = (TAsy::Part("PARA", "POS", c_Beta_BPARA_TPOS)).at(1);
  c_Beta_BPARA_TNEG_back = (TAsy::Part("PARA", "NEG", c_Beta_BPARA_TNEG)).at(1);


  // //_______________
  // vector<Double_t> KB1 = bindet->ReturnKBvec("gammaE");
  // Int_t KB1size = KB1.size();
  // Int_t gammaindex = index/KB1size;

  // string mmc_table("/Users/yuqing/work/stage3/kbplot/finals_test/mm_cut_table/range.mmc");
  // vector<Double_t> mmc = GetMMC2(mmc_table, gammaindex+1, Topo);
  // Double_t mean = mmc[0]-mmc[1]*2;
  // Double_t sigma = mmc[0]+mmc[1]*2;

  // Double_t dMMB, dMMC;
  // Double_t MMB = bb->IntegralAndError(bb->FindBin(mean-sigma),bb->FindBin(mean+sigma), dMMB);
  // Double_t MMC = cc->IntegralAndError(cc->FindBin(mean-sigma),cc->FindBin(mean+sigma), dMMC);
  // Double_t alpha = PAR[3];
  // Double_t alphaE = PARERROR[3];

  // cout<<b_Beta_BPERP_TPOS_adjusted->Integral()<<"    "<<b_MM_BPERP_TPOS_adjusted->Integral(b_MM_BPERP_TPOS_adjusted->FindBin(left),b_MM_BPERP_TPOS_adjusted->FindBin(right));
  // b_Beta_BPERP_TNEG_adjusted->Integral();
  // b_Beta_BPARA_TPOS_adjusted->Integral();
  // b_Beta_BPARA_TNEG_adjusted->Integral();
  // c_Beta_BPERP_TPOS_adjusted->Integral();
  // c_Beta_BPERP_TNEG_adjusted->Integral();
  // c_Beta_BPARA_TPOS_adjusted->Integral();
  // c_Beta_BPARA_TNEG_adjusted->Integral();



  //_______________




  if(OL){
    b_Beta_BPERP_TPOS_i1 = (TH1F*)TAsy::Overlap("PERP", "POS", b_Beta_BPERP_TPOS);
    b_Beta_BPERP_TNEG_i1 = (TH1F*)TAsy::Overlap("PERP", "NEG", b_Beta_BPERP_TNEG);
    b_Beta_BPARA_TPOS_i1 = (TH1F*)TAsy::Overlap("PARA", "POS", b_Beta_BPARA_TPOS);
    b_Beta_BPARA_TNEG_i1 = (TH1F*)TAsy::Overlap("PARA", "NEG", b_Beta_BPARA_TNEG);
    c_Beta_BPERP_TPOS_i1 = (TH1F*)TAsy::Overlap("PERP", "POS", c_Beta_BPERP_TPOS);
    c_Beta_BPERP_TNEG_i1 = (TH1F*)TAsy::Overlap("PERP", "NEG", c_Beta_BPERP_TNEG);
    c_Beta_BPARA_TPOS_i1 = (TH1F*)TAsy::Overlap("PARA", "POS", c_Beta_BPARA_TPOS);
    c_Beta_BPARA_TNEG_i1 = (TH1F*)TAsy::Overlap("PARA", "NEG", c_Beta_BPARA_TNEG);
  }
  else{
    b_Beta_BPERP_TPOS_i1 = (TH1F*)b_Beta_BPERP_TPOS->Clone("");
    b_Beta_BPERP_TNEG_i1 = (TH1F*)b_Beta_BPERP_TNEG->Clone("");
    b_Beta_BPARA_TPOS_i1 = (TH1F*)b_Beta_BPARA_TPOS->Clone("");
    b_Beta_BPARA_TNEG_i1 = (TH1F*)b_Beta_BPARA_TNEG->Clone(""); 
    c_Beta_BPERP_TPOS_i1 = (TH1F*)c_Beta_BPERP_TPOS->Clone("");
    c_Beta_BPERP_TNEG_i1 = (TH1F*)c_Beta_BPERP_TNEG->Clone("");
    c_Beta_BPARA_TPOS_i1 = (TH1F*)c_Beta_BPARA_TPOS->Clone("");
    c_Beta_BPARA_TNEG_i1 = (TH1F*)c_Beta_BPARA_TNEG->Clone("");
  }



  //______________rebin
  b_Beta_BPERP_TPOS_i1 = TAsy::DynamicRebin( b_Beta_BPERP_TPOS_i1, rebin, fiducialcut);
  b_Beta_BPERP_TNEG_i1 = TAsy::DynamicRebin( b_Beta_BPERP_TNEG_i1, rebin, fiducialcut);
  b_Beta_BPARA_TPOS_i1 = TAsy::DynamicRebin( b_Beta_BPARA_TPOS_i1, rebin, fiducialcut);
  b_Beta_BPARA_TNEG_i1 = TAsy::DynamicRebin( b_Beta_BPARA_TNEG_i1, rebin, fiducialcut);

  c_Beta_BPERP_TPOS_i1 = TAsy::DynamicRebin( c_Beta_BPERP_TPOS_i1, rebin, fiducialcut);
  c_Beta_BPERP_TNEG_i1 = TAsy::DynamicRebin( c_Beta_BPERP_TNEG_i1, rebin, fiducialcut);
  c_Beta_BPARA_TPOS_i1 = TAsy::DynamicRebin( c_Beta_BPARA_TPOS_i1, rebin, fiducialcut);
  c_Beta_BPARA_TNEG_i1 = TAsy::DynamicRebin( c_Beta_BPARA_TNEG_i1, rebin, fiducialcut);




  // b_Beta_BPERP_TPOS_adjusted->Rebin(250);
  // b_Beta_BPERP_TNEG_adjusted->Rebin(250);
  // b_Beta_BPARA_TPOS_adjusted->Rebin(250);
  // b_Beta_BPARA_TNEG_adjusted->Rebin(250);

  // c_Beta_BPERP_TPOS_adjusted->Rebin(250);
  // c_Beta_BPERP_TNEG_adjusted->Rebin(250);
  // c_Beta_BPARA_TPOS_adjusted->Rebin(250);
  // c_Beta_BPARA_TNEG_adjusted->Rebin(250);



  //______________scale
  // Double_t perp_pos = 1;
  // Double_t para_pos = 1/(norm_para_pos/norm_perp_pos);
  // Double_t perp_neg = 1/(norm_perp_neg/norm_perp_pos);
  // Double_t para_neg = 1/(norm_para_neg/norm_perp_pos);

  Double_t perp_pos = 1/(norm_perp_pos/norm_para_neg);
  Double_t para_pos = 1/(norm_para_pos/norm_para_neg);
  Double_t perp_neg = 1/(norm_perp_neg/norm_para_neg);
  Double_t para_neg = 1/(norm_para_neg/norm_para_neg);

  b_Beta_BPERP_TPOS_i1 = TAsy::DynamicNorm(b_Beta_BPERP_TPOS_i1, perp_pos );
  b_Beta_BPARA_TPOS_i1 = TAsy::DynamicNorm(b_Beta_BPARA_TPOS_i1, para_pos );
  b_Beta_BPERP_TNEG_i1 = TAsy::DynamicNorm(b_Beta_BPERP_TNEG_i1, perp_neg );
  b_Beta_BPARA_TNEG_i1 = TAsy::DynamicNorm(b_Beta_BPARA_TNEG_i1, para_neg );

  c_Beta_BPERP_TPOS_i1 = TAsy::DynamicNorm(c_Beta_BPERP_TPOS_i1, perp_pos );
  c_Beta_BPARA_TPOS_i1 = TAsy::DynamicNorm(c_Beta_BPARA_TPOS_i1, para_pos );
  c_Beta_BPERP_TNEG_i1 = TAsy::DynamicNorm(c_Beta_BPERP_TNEG_i1, perp_neg );
  c_Beta_BPARA_TNEG_i1 = TAsy::DynamicNorm(c_Beta_BPARA_TNEG_i1, para_neg );




  ///////////////////////


  b_Beta_BPERP_TPOS_adjusted = (TH1F*)b_Beta_BPERP_TPOS_i1->Clone("");
  b_Beta_BPERP_TNEG_adjusted = (TH1F*)b_Beta_BPERP_TNEG_i1->Clone("");
  b_Beta_BPARA_TPOS_adjusted = (TH1F*)b_Beta_BPARA_TPOS_i1->Clone("");
  b_Beta_BPARA_TNEG_adjusted = (TH1F*)b_Beta_BPARA_TNEG_i1->Clone(""); 
  c_Beta_BPERP_TPOS_adjusted = (TH1F*)c_Beta_BPERP_TPOS_i1->Clone("");
  c_Beta_BPERP_TNEG_adjusted = (TH1F*)c_Beta_BPERP_TNEG_i1->Clone("");
  c_Beta_BPARA_TPOS_adjusted = (TH1F*)c_Beta_BPARA_TPOS_i1->Clone("");
  c_Beta_BPARA_TNEG_adjusted = (TH1F*)c_Beta_BPARA_TNEG_i1->Clone("");






  //______________flip
  b_Beta_BPARA_TPOS_adjusted = TAsy::Flip_Ninty_Deg(b_Beta_BPARA_TPOS_adjusted);
  b_Beta_BPARA_TNEG_adjusted = TAsy::Flip_Ninty_Deg(b_Beta_BPARA_TNEG_adjusted);

  c_Beta_BPARA_TPOS_adjusted = TAsy::Flip_Ninty_Deg(c_Beta_BPARA_TPOS_adjusted);
  c_Beta_BPARA_TNEG_adjusted = TAsy::Flip_Ninty_Deg(c_Beta_BPARA_TNEG_adjusted);







  Double_t Ba = b_Beta_BPERP_TPOS_adjusted->Integral();
  Double_t Bb = b_Beta_BPARA_TPOS_adjusted->Integral();
  Double_t Bc = b_Beta_BPERP_TNEG_adjusted->Integral();
  Double_t Bd = b_Beta_BPARA_TNEG_adjusted->Integral();

  Double_t Ca = c_Beta_BPERP_TPOS_adjusted->Integral();
  Double_t Cb = c_Beta_BPARA_TPOS_adjusted->Integral();
  Double_t Cc = c_Beta_BPERP_TNEG_adjusted->Integral();
  Double_t Cd = c_Beta_BPARA_TNEG_adjusted->Integral();

  dilu4v.push_back(Ba);
  dilu4v.push_back(Bb);
  dilu4v.push_back(Bc);
  dilu4v.push_back(Bd);

  dilu4v.push_back(Ca);
  dilu4v.push_back(Cb);
  dilu4v.push_back(Cc);
  dilu4v.push_back(Cd);




  b_MM_BPERP_TPOS_adjusted = (TH1F*)b_MM_BPERP_TPOS->Clone("");
  b_MM_BPERP_TNEG_adjusted = (TH1F*)b_MM_BPERP_TNEG->Clone("");
  b_MM_BPARA_TPOS_adjusted = (TH1F*)b_MM_BPARA_TPOS->Clone("");
  b_MM_BPARA_TNEG_adjusted = (TH1F*)b_MM_BPARA_TNEG->Clone(""); 
  c_MM_BPERP_TPOS_adjusted = (TH1F*)c_MM_BPERP_TPOS->Clone("");
  c_MM_BPERP_TNEG_adjusted = (TH1F*)c_MM_BPERP_TNEG->Clone("");
  c_MM_BPARA_TPOS_adjusted = (TH1F*)c_MM_BPARA_TPOS->Clone("");
  c_MM_BPARA_TNEG_adjusted = (TH1F*)c_MM_BPARA_TNEG->Clone("");

  Double_t left,right;
  if(Topo==1){left=0.8;right=1;}
  else if(Topo==2){left=0.8;right=1;}
  else if(Topo==3){left=0.;right=0.1;}
  else if(Topo==4){left=0.;right=0.1;}

  // Double_t rm1 = c_MM_BPERP_TPOS->Integral(c_MM_BPERP_TPOS->FindBin(left),c_MM_BPERP_TPOS->FindBin(right)); 
  // Double_t rm2 = c_MM_BPARA_TPOS->Integral(c_MM_BPARA_TPOS->FindBin(left),c_MM_BPARA_TPOS->FindBin(right)); 
  // Double_t rm3 = c_MM_BPERP_TNEG->Integral(c_MM_BPERP_TNEG->FindBin(left),c_MM_BPERP_TNEG->FindBin(right)); 
  // Double_t rm4 = c_MM_BPARA_TNEG->Integral(c_MM_BPARA_TNEG->FindBin(left),c_MM_BPARA_TNEG->FindBin(right)); 

  Double_t rm1 = c_MM_BPERP_TPOS->Integral(0,10000); 
  Double_t rm2 = c_MM_BPARA_TPOS->Integral(0,10000); 
  Double_t rm3 = c_MM_BPERP_TNEG->Integral(0,10000); 
  Double_t rm4 = c_MM_BPARA_TNEG->Integral(0,10000); 
  normcheck.push_back(rm1);
  normcheck.push_back(rm2);
  normcheck.push_back(rm3);
  normcheck.push_back(rm4);



  b_Beta_BPERP_TPOS_front ->Rebin(rebin);
  b_Beta_BPERP_TNEG_front ->Rebin(rebin); 
  b_Beta_BPARA_TPOS_front ->Rebin(rebin);
  b_Beta_BPARA_TNEG_front ->Rebin(rebin);
  c_Beta_BPERP_TPOS_front ->Rebin(rebin);
  c_Beta_BPERP_TNEG_front ->Rebin(rebin);
  c_Beta_BPARA_TPOS_front ->Rebin(rebin);
  c_Beta_BPARA_TNEG_front ->Rebin(rebin);

  b_Beta_BPERP_TPOS_back ->Rebin(rebin);
  b_Beta_BPERP_TNEG_back ->Rebin(rebin); 
  b_Beta_BPARA_TPOS_back ->Rebin(rebin);
  b_Beta_BPARA_TNEG_back ->Rebin(rebin);
  c_Beta_BPERP_TPOS_back ->Rebin(rebin);
  c_Beta_BPERP_TNEG_back ->Rebin(rebin);
  c_Beta_BPARA_TPOS_back ->Rebin(rebin);
  c_Beta_BPARA_TNEG_back ->Rebin(rebin);

  betastats = TAsy::BetaStats(b_Beta_BPERP_TPOS_adjusted, b_Beta_BPERP_TNEG_adjusted,
			      b_Beta_BPARA_TPOS_adjusted, b_Beta_BPARA_TNEG_adjusted,
			      c_Beta_BPERP_TPOS_adjusted, c_Beta_BPERP_TNEG_adjusted,
			      c_Beta_BPARA_TPOS_adjusted, c_Beta_BPARA_TNEG_adjusted);
  

  // cmm_kb[1] = cmm_kb[1]/(entry_para_pos/entry_perp_pos);
  // cmm_kb[2] = cmm_kb[2]/(entry_perp_neg/entry_perp_pos);
  // cmm_kb[3] = cmm_kb[3]/(entry_para_neg/entry_perp_pos);


  // Double_t para_pos = 1;
  // Double_t para_neg = 1/(norm_para_pos/norm_perp_pos);
  // Double_t perp_pos = 1/(norm_perp_neg/norm_perp_pos);
  // Double_t perp_neg = 1/(norm_para_neg/norm_perp_pos);


  b_MM_BPERP_TPOS_adjusted->Scale(perp_pos);
  b_MM_BPARA_TPOS_adjusted->Scale(para_pos);
  b_MM_BPERP_TNEG_adjusted->Scale(perp_neg);
  b_MM_BPARA_TNEG_adjusted->Scale(para_neg);
  c_MM_BPERP_TPOS_adjusted->Scale(perp_pos);
  c_MM_BPARA_TPOS_adjusted->Scale(para_pos);
  c_MM_BPERP_TNEG_adjusted->Scale(perp_neg);
  c_MM_BPARA_TNEG_adjusted->Scale(para_neg);

  // b_Beta_BPERP_TPOS_adjusted->Scale(1);
  // b_Beta_BPERP_TNEG_adjusted->Scale(perp_neg/perp_pos);
  // b_Beta_BPARA_TPOS_adjusted->Scale(1);
  // b_Beta_BPARA_TNEG_adjusted->Scale(para_neg/para_pos);
  // c_Beta_BPERP_TPOS_adjusted->Scale(1);
  // c_Beta_BPERP_TNEG_adjusted->Scale(perp_neg/perp_pos);
  // c_Beta_BPARA_TPOS_adjusted->Scale(1);
  // c_Beta_BPARA_TNEG_adjusted->Scale(para_neg/para_pos);

  b_Beta_BPERP_TPOS_front->Scale(perp_pos);
  b_Beta_BPERP_TNEG_front->Scale(perp_neg);
  b_Beta_BPARA_TPOS_front->Scale(para_pos);
  b_Beta_BPARA_TNEG_front->Scale(para_neg);
  c_Beta_BPERP_TPOS_front->Scale(perp_pos);
  c_Beta_BPERP_TNEG_front->Scale(perp_neg);
  c_Beta_BPARA_TPOS_front->Scale(para_pos);
  c_Beta_BPARA_TNEG_front->Scale(para_neg);

  b_Beta_BPERP_TPOS_back->Scale(perp_pos);
  b_Beta_BPERP_TNEG_back->Scale(perp_neg);
  b_Beta_BPARA_TPOS_back->Scale(para_pos);
  b_Beta_BPARA_TNEG_back->Scale(para_neg);
  c_Beta_BPERP_TPOS_back->Scale(perp_pos);
  c_Beta_BPERP_TNEG_back->Scale(perp_neg);
  c_Beta_BPARA_TPOS_back->Scale(para_pos);
  c_Beta_BPARA_TNEG_back->Scale(para_neg);

  valid_bin_perp = b_Beta_BPERP_TPOS_adjusted->GetXaxis()->GetNbins() - excluded_bin_perp.size();
  valid_bin_para = b_Beta_BPARA_TPOS_adjusted->GetXaxis()->GetNbins() - excluded_bin_para.size();
  return this;
}



vector<Double_t> TAsy::GetPolValues(string tarP){
  vector<Double_t> ret;
  ifstream read_in((inputfiledir+inputfilename+".out").c_str());
  string first;
  string vv, SS;
  Double_t SFvalue;
  while(getline(read_in,first)){
    string::size_type findtopo = first.find("Topology:",0);
    if(findtopo!=string::npos){
      string s1,s2,s3,s4;
      Int_t i1,i2;
      istringstream iss(first);
      iss>>s1>>i1>>s2>>i2>>s3>>s4;
      if(i1 == index && i2 == Topo && s4 == tarP){
	string BP,TP;
	Double_t bp,tp;
	read_in>>BP>>bp;
	read_in>>TP>>tp;
	ret.push_back(bp);
	ret.push_back(tp);
  	break;
      } 
    }
  }
  if(Int_t(ret.size())==0) cout<<"Did not find pol. information    BinIndex: "<<index<<"  Topo: "<<Topo<<endl;
  read_in.close();
  return ret;
}





TGraphErrors* TAsy::GetAsy(TH1F* b1, TH1F* b2, TH1F* c1, TH1F* c2, string name, string beam_pol){
  TGraphErrors* asy = new TGraphErrors();
  ifstream ff1("/Users/yuqing/work/stage3/pass1/table.txt" );
  string first;
  string vv, SS;
  Double_t SFvalue;
  while(getline(ff1,first)){
    string::size_type findtopo = first.find("Topology:",0);
    if(findtopo!=string::npos){
      string a = first.substr(findtopo+13,1);
      string b = first.substr(findtopo+26,3);
      string c = first.substr(findtopo+41,7);
      istringstream iss1(a);
      istringstream iss2(b);
      Int_t ss1,ss2;
      iss1>>ss1;
      iss2>>ss2;
      string::size_type findperp = first.find(beam_pol,0);
      if(ss1 == Topo && ss2 == index && findperp!=string::npos){
  	getline(ff1,first);
  	ff1>>SS>>SFvalue;
  	break;
      } 
    }
  }
  TH1F* B1 = new TH1F();
  TH1F* B2 = new TH1F();
  TH1F* C1 = new TH1F();
  TH1F* C2 = new TH1F();
  B1 = (TH1F*)b1->Clone("B1");
  B2 = (TH1F*)b2->Clone("B2");
  C1 = (TH1F*)c1->Clone("C1");
  C2 = (TH1F*)c2->Clone("C2");
  Int_t binnum = B1->GetXaxis()->GetNbins();
  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t v_B1 = B1->GetBinContent(i);
    Double_t v_B2 = B2->GetBinContent(i); 
    Double_t v_C1 = C1->GetBinContent(i);
    Double_t v_C2 = C2->GetBinContent(i);
    Double_t v_C = v_C1+v_C2;
    Double_t e_B1 = B1->GetBinError(i);
    Double_t e_B2 = B2->GetBinError(i);
    Double_t e_C1 = C1->GetBinError(i);
    Double_t e_C2 = C2->GetBinError(i);
    Double_t e_C = sqrt(e_C1*e_C1+e_C2*e_C2);

    if(v_B1==0. && v_B2==0.){
      continue;
    }

    Double_t v_upper = v_B1-v_B2;
    Double_t v_low = v_B1+v_B2-SFvalue*v_C;
    Double_t BinAsy = v_upper/v_low;
    Double_t Fac1 = 1/((v_B1+v_B2-v_C)*(v_B1+v_B2-v_C));
    Double_t Fac2_B1 = (e_B1*(2*v_B2-v_C))*(e_B1*(2*v_B2-v_C));
    Double_t Fac2_B2 = (e_B1*(2*v_B2-v_C))*(e_B1*(2*v_B2-v_C));
    Double_t Fac2_C = (e_C*(v_B1-v_B2))*(e_C*(v_B1-v_B2));
    Double_t Fac2 = sqrt(Fac2_B1 + Fac2_B2 + Fac2_C);
    Double_t BinAsyErr = Fac1*Fac2;
    Double_t BinCenter = B1->GetBinCenter(i);
    Double_t BinWidth = B1->GetXaxis()->GetBinWidth(i)/2;

    asy->SetPoint(ii, BinCenter, BinAsy);
    asy->SetPointError(ii, BinWidth, BinAsyErr);
    ii++;
  }

  asy->GetXaxis()->SetTitle("#beta   (Rad)");
  asy->GetYaxis()->SetTitle("Asymmetry");
  // TCanvas *cc = new TCanvas("cc","cc",2500,1500);
  // asy->Draw("AP");
  // cc->Print("a.png");
  // if(beam_pol == "PERP"){
  //   for(vector<Int_t>::size_type i=0;i<excluded_bin_perp.size();i++){
  //     // asy->SetBinContent(excluded_bin_perp[i],0);
  //     // asy->SetBinError(excluded_bin_perp[i],0.0);
  //   }
  // }
  // if(beam_pol == "PARA"){
  //   for(vector<Int_t>::size_type i=0;i<excluded_bin_para.size();i++){
  //     // asy->SetBinContent(excluded_bin_para[i],0);
  //     // asy->SetBinError(excluded_bin_para[i],0.0);
  //   }
  // }
  // delete B1;
  // delete B2;
  return asy;
}


TGraphErrors* TAsy::GetAsy(TH1F* b1, TH1F* b2, 
			   TH1F* c_pos_perp, TH1F* c_neg_perp, TH1F* c_pos_para, TH1F* c_neg_para,
			   string name, string beam_pol){
  TGraphErrors* asy = new TGraphErrors();

  TH1F* c_perp = new TH1F();
  c_perp = (TH1F*)c_pos_perp->Clone();
  c_perp->Add(c_neg_perp,1);
  TH1F* c_para = new TH1F();
  c_para = (TH1F*)c_pos_para->Clone();
  c_para->Add(c_neg_para,1);

  ifstream ff1("/Users/yuqing/work/stage3/pass1/table.txt");
  string first;
  string vv, SS;
  Double_t SFvalue_perp;
  Double_t SFvalue_para;
  Bool_t p1f(false);
  Bool_t p2f(false);
  while(getline(ff1,first)){
    string::size_type findtopo = first.find("Topology:",0);
    if(findtopo!=string::npos){
      string a = first.substr(findtopo+13,1);
      string b = first.substr(findtopo+26,3);
      string c = first.substr(findtopo+41,7);
      istringstream iss1(a);
      istringstream iss2(b);
      Int_t ss1,ss2;
      iss1>>ss1;
      iss2>>ss2;
      string::size_type findperp = first.find("perp",0);
      string::size_type findpara = first.find("para",0);
      if(ss1 == Topo && ss2 == index && findperp!=string::npos){
  	getline(ff1,first);
  	ff1>>SS>>SFvalue_perp;
	p1f = true;
      } 
      else if(ss1 == Topo && ss2 == index && findpara!=string::npos){
  	getline(ff1,first);
  	ff1>>SS>>SFvalue_para;
	p2f = true;
      } 
      if(p1f && p2f) break;
    }
  }

  TH1F* B1 = new TH1F();
  TH1F* B2 = new TH1F();
  TH1F* C1 = new TH1F();
  TH1F* C2 = new TH1F();
  B1 = (TH1F*)b1->Clone("B1");
  B2 = (TH1F*)b2->Clone("B2");
  C1 = (TH1F*)c_perp->Clone("C1");
  C2 = (TH1F*)c_para->Clone("C2");
  Int_t binnum = B1->GetXaxis()->GetNbins();
  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t v_B1 = B1->GetBinContent(i);
    Double_t v_B2 = B2->GetBinContent(i); 
    Double_t v_C1 = C1->GetBinContent(i);
    Double_t v_C2 = C2->GetBinContent(i);
    Double_t v_C = v_C1+v_C2;
    Double_t e_B1 = B1->GetBinError(i);
    Double_t e_B2 = B2->GetBinError(i);
    Double_t e_C1 = C1->GetBinError(i);
    Double_t e_C2 = C2->GetBinError(i);
    Double_t e_C = sqrt(e_C1*e_C1+e_C2*e_C2);

    if(v_B1==0. && v_B2==0.){
      continue;
    }

    // SFvalue_perp = 5.8;
    // SFvalue_para = 5.8;


    Double_t v_upper = v_B1-v_B2;
    Double_t v_low = (v_B1+v_B2); //-SFvalue_perp*v_C1-SFvalue_para*v_C2;
    Double_t BinAsy = v_upper/v_low;
    // Double_t BinAsy = (v_B1-v_B2)/(v_B1+v_B2);
    Double_t Fac1 = 1/((v_B1+v_B2-v_C)*(v_B1+v_B2-v_C));
    // Double_t Fac1 = 1/(v_low*v_low);
    Double_t Fac2_B1 = (e_B1*(2*v_B2-v_C))*(e_B1*(2*v_B2-v_C));

    Double_t Fac2_B2 = (e_B1*(2*v_B2-v_C))*(e_B1*(2*v_B2-v_C));
    Double_t Fac2_C = (e_C*(v_B1-v_B2))*(e_C*(v_B1-v_B2));
    Double_t Fac2 = sqrt(Fac2_B1 + Fac2_B2 + Fac2_C);
    Double_t BinAsyErr = Fac1*Fac2;
    Double_t BinCenter = B1->GetBinCenter(i);
    Double_t BinWidth = B1->GetXaxis()->GetBinWidth(i)/2;

    /////all sigmas
    Double_t sigmaB1 = e_B1;
    Double_t sigmaB2 = e_B2;
    Double_t sigmaC1 = e_C1;
    Double_t sigmaC2 = e_C2;
    Double_t sigmaSFperp = SFvalue_perp/2;
    Double_t sigmaSFpara = SFvalue_para/2;
    /////denominator
    Double_t deno = v_low*v_low;
    /////all partial derivatives
    Double_t partialB1 = (2*v_B2-SFvalue_perp*v_C1-SFvalue_para*v_C2)/deno;
    Double_t partialB2 = (-2*v_B1+SFvalue_perp*v_C1+SFvalue_para*v_C2)/deno;
    Double_t partialSFperp = v_C1*(v_B1-v_B2)/deno;
    Double_t partialSFpara = v_C2*(v_B1-v_B2)/deno;
    Double_t partialC1 = SFvalue_perp*(v_B1-v_B2)/deno;
    Double_t partialC2 = SFvalue_para*(v_B1-v_B2)/deno;
    /////error
    Double_t err = sqrt(sigmaB1*sigmaB1*partialB1*partialB1+
			sigmaB2*sigmaB2*partialB2*partialB2+
			sigmaC1*sigmaC1*partialC1*partialC1+
			sigmaC2*sigmaC2*partialC2*partialC2+
			sigmaSFperp*sigmaSFperp*partialSFperp*partialSFperp+
			sigmaSFpara*sigmaSFpara*partialSFpara*partialSFpara);

    vector<Double_t> pospol = TAsy::GetPolValues("pos");
    vector<Double_t> negpol = TAsy::GetPolValues("neg");
    Double_t bp_pos = fabs(pospol[0]);
    Double_t tp_pos = fabs(pospol[1]);
    Double_t bp_neg = fabs(negpol[0]);
    Double_t tp_neg = fabs(negpol[1]);

    Double_t r = bp_pos/bp_neg;
    r = r;
    // BinAsy = BinAsy - (1-r)/2;

    Double_t bp = bp_pos;
    Double_t tp = (tp_pos+tp_neg)/2;

    BinAsy = (BinAsy/bp)/tp;

    BinAsyErr = err;
    asy->SetPoint(ii, BinCenter, BinAsy);
    asy->SetPointError(ii, BinWidth, BinAsyErr);
    ii++;
  }

  asy->GetXaxis()->SetTitle("#beta   (Rad)");
  asy->GetYaxis()->SetTitle("Asymmetry");
  return asy;
}



TGraphErrors* TAsy::GetAsyDF(TH1F* perppos, TH1F* perpneg, TH1F* parapos, TH1F* paraneg, string name){
  TGraphErrors* asy = new TGraphErrors();
  TH1F* B1perp = new TH1F();
  TH1F* B2perp = new TH1F();
  TH1F* B1para = new TH1F();
  TH1F* B2para = new TH1F();
  B1perp = (TH1F*)perppos->Clone("B1perp");
  B2perp = (TH1F*)perpneg->Clone("B2perp");
  B1para = (TH1F*)parapos->Clone("B1para");
  B2para = (TH1F*)paraneg->Clone("B2para");
  Int_t binnum = B1perp->GetXaxis()->GetNbins();
  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t v_B1perp = B1perp->GetBinContent(i);
    Double_t v_B2perp = B2perp->GetBinContent(i);
    Double_t e_B1perp = B1perp->GetBinError(i);
    Double_t e_B2perp = B2perp->GetBinError(i);
    Double_t v_B1para = B1para->GetBinContent(i);
    Double_t v_B2para = B2para->GetBinContent(i);
    Double_t e_B1para = B1para->GetBinError(i);
    Double_t e_B2para = B2para->GetBinError(i);
    if(v_B1perp==0. && v_B2perp==0. && v_B1para==0. && v_B2para==0.) continue;
    Double_t BinAsy_perp = (v_B2perp-v_B1perp)/(v_B1perp+v_B2perp);
    Double_t BinAsy_para = (v_B2para-v_B1para)/(v_B1para+v_B2para);
    Double_t BinCenter = B1perp->GetBinCenter(i);
    Double_t BinWidth = B1perp->GetXaxis()->GetBinWidth(i)/2;
    /////error
    Double_t err_perp = (2/((v_B1perp+v_B2perp)*(v_B1perp+v_B2perp)))
      *sqrt(e_B1perp*e_B1perp*v_B2perp*v_B2perp+e_B2perp*e_B2perp*v_B1perp*v_B1perp);
    Double_t err_para = (2/((v_B1para+v_B2para)*(v_B1para+v_B2para)))
      *sqrt(e_B1para*e_B1para*v_B2para*v_B2para+e_B2para*e_B2para*v_B1para*v_B1para);
    Double_t w_perp = 1/(err_perp*err_perp);
    Double_t w_para = 1/(err_para*err_para);
    Double_t w = w_perp + w_para;
    Double_t BinAsy = (1/w)*(BinAsy_perp*w_perp + BinAsy_para*w_para);
    Double_t BinAsyErr = 1/sqrt(w);

    asy->SetPoint(ii, BinCenter, BinAsy);
    asy->SetPointError(ii, BinWidth, BinAsyErr);
    ii++;
  }
  asy->GetXaxis()->SetTitle("#beta   (Rad)");
  asy->GetYaxis()->SetTitle("Asymmetry");
  return asy;
}



TGraphErrors* TAsy::GetAsyCarbon(TH1F* c1, TH1F* c2){
  TGraphErrors* asy = new TGraphErrors();
  TH1F* C1 = new TH1F();
  TH1F* C2 = new TH1F();
  C1 = (TH1F*)c1->Clone("C1");
  C2 = (TH1F*)c2->Clone("C2");
  Int_t binnum = C1->GetXaxis()->GetNbins();
  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t v_C1 = C1->GetBinContent(i);
    Double_t v_C2 = C2->GetBinContent(i);
    Double_t e_C1 = C1->GetBinError(i);
    Double_t e_C2 = C2->GetBinError(i);
    if(v_C1==0. && v_C2==0.){
      continue;
    }
    Double_t v_upper = v_C1-v_C2;
    Double_t v_low = v_C1+v_C2;
    Double_t BinAsy = v_upper/v_low;
    Double_t Fac1 = 2/((v_C1+v_C2)*(v_C1+v_C2));
    Double_t Fac2 = sqrt((e_C1*e_C1*v_C2*v_C2)+(e_C2*e_C2*v_C1*v_C1));
    Double_t BinAsyErr = Fac1*Fac2;
    Double_t BinCenter = C1->GetBinCenter(i);
    Double_t BinWidth = C1->GetXaxis()->GetBinWidth(i)/2;
    asy->SetPoint(ii, BinCenter, BinAsy);
    asy->SetPointError(ii, BinWidth, BinAsyErr);
    ii++;
  }
  asy->GetXaxis()->SetTitle("#beta   (Rad)");
  asy->GetYaxis()->SetTitle("Asymmetry");
  return asy;
}


TGraphErrors* TAsy::plus(TH1F* c1, TH1F* c2){
  TGraphErrors* asy = new TGraphErrors();
  TH1F* C1 = new TH1F();
  TH1F* C2 = new TH1F();
  C1 = (TH1F*)c1->Clone("C1");
  C2 = (TH1F*)c2->Clone("C2");
  Int_t binnum = C1->GetXaxis()->GetNbins();
  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t v_C1 = C1->GetBinContent(i);
    Double_t v_C2 = C2->GetBinContent(i);
    Double_t e_C1 = C1->GetBinError(i);
    Double_t e_C2 = C2->GetBinError(i);
    if(v_C1==0. && v_C2==0.){
      continue;
    }

    Double_t v_low = v_C1+v_C2;
    Double_t BinAsy = v_low;
    Double_t BinAsyErr = sqrt(v_low);
    Double_t BinCenter = C1->GetBinCenter(i);
    Double_t BinWidth = C1->GetXaxis()->GetBinWidth(i)/2;
    asy->SetPoint(ii, BinCenter, BinAsy);
    asy->SetPointError(ii, BinWidth, BinAsyErr);
    ii++;
  }
  asy->GetXaxis()->SetTitle("#beta   (Rad)");
  asy->GetYaxis()->SetTitle("Asymmetry");
  return asy; 
}


TAsy* TAsy::GetAsyAll(Bool_t combine_perp_para, Double_t phi0){



  TAsy::GetPZDILU();
  TAsy::GetRawDILU();



  combine = combine_perp_para;
  Int_t bins = beta_bin_nums_origin;


  TH1F* b_pos = new TH1F();
  b_pos = (TH1F*)b_Beta_BPERP_TPOS_adjusted->Clone();
  b_pos->Add(b_Beta_BPARA_TPOS_adjusted,1);
  TH1F* b_neg = new TH1F();
  b_neg = (TH1F*)b_Beta_BPERP_TNEG_adjusted->Clone();
  b_neg->Add(b_Beta_BPARA_TNEG_adjusted,1);



  TH1F* c_pos_perp = new TH1F();
  c_pos_perp = (TH1F*)c_Beta_BPERP_TPOS_adjusted->Clone();
  TH1F* c_neg_perp = new TH1F();
  c_neg_perp = (TH1F*)c_Beta_BPERP_TNEG_adjusted->Clone();
  // c_neg->Add(c_Beta_BPARA_TNEG_adjusted,1);

  TH1F* c_pos_para = new TH1F();
  c_pos_para = (TH1F*)c_Beta_BPARA_TPOS_adjusted->Clone();
  TH1F* c_neg_para = new TH1F();
  c_neg_para = (TH1F*)c_Beta_BPARA_TNEG_adjusted->Clone();

  TH1F* c_pos = new TH1F();
  TH1F* c_neg = new TH1F();
  c_pos = (TH1F*)c_Beta_BPERP_TPOS_adjusted->Clone();
  c_neg = (TH1F*)c_Beta_BPERP_TNEG_adjusted->Clone();
  c_pos->Add(c_pos_para,1);
  c_neg->Add(c_neg_para,1);
  // asy_combine = TAsy::GetAsy(b_pos, b_neg, c_pos, c_neg,"asy_combine","PERP");
  asy_carbon = TAsy::GetAsyCarbon(c_pos, c_neg);


  // vector<Double_t> carnorm = TAsy::GetCarbonNorm(asy_carbon); 
  // b_pos->Scale(1.);
  // b_neg->Scale(carnorm[0]);
  // c_pos_perp->Scale(1.);
  // c_neg_perp->Scale(carnorm[0]);
  // c_pos_para->Scale(1.);
  // c_neg_para->Scale(carnorm[0]);

  TH1F* b_pos_norm = new TH1F();
  b_pos_norm = (TH1F*)b_Beta_BPERP_TPOS_adjusted->Clone();
  TH1F* b_neg_norm = new TH1F();
  b_neg_norm = (TH1F*)b_Beta_BPERP_TNEG_adjusted->Clone();
  b_pos_norm->Scale(norm_para_pos);
  b_neg_norm->Scale(norm_para_pos);
  posNneg = TAsy::plus(b_pos_norm,b_neg_norm);




  //asy_combine = TAsy::GetAsy(b_neg, b_pos, c_pos_perp, c_neg_perp, c_pos_para, c_neg_para,"asy_combine","PERP");

  // asy_combine = TAsy::GetAsy(b_neg, b_pos, c_pos_perp, c_neg_perp, c_pos_para, c_neg_para,"asy_combine","PERP");
  asy_combine = TAsy::GetAsyDF(b_Beta_BPERP_TPOS_adjusted, b_Beta_BPERP_TNEG_adjusted,
			       b_Beta_BPARA_TPOS_adjusted, b_Beta_BPARA_TNEG_adjusted,"asy_combine");




  // TH1F* b_Beta_BPARA_TPOS_adjusted_flip = new TH1F();
  // TH1F* b_Beta_BPARA_TNEG_adjusted_flip = new TH1F();
  // b_Beta_BPARA_TPOS_adjusted_flip = TAsy::Flip_Ninty_Deg(b_Beta_BPARA_TPOS_adjusted);
  // b_Beta_BPARA_TNEG_adjusted_flip = TAsy::Flip_Ninty_Deg(b_Beta_BPARA_TNEG_adjusted);

  // TH1F* c_Beta_BPARA_TPOS_adjusted_flip = new TH1F();
  // TH1F* c_Beta_BPARA_TNEG_adjusted_flip = new TH1F();
  // c_Beta_BPARA_TPOS_adjusted_flip = TAsy::Flip_Ninty_Deg(c_Beta_BPARA_TPOS_adjusted);
  // c_Beta_BPARA_TNEG_adjusted_flip = TAsy::Flip_Ninty_Deg(c_Beta_BPARA_TNEG_adjusted);

  b_Beta_BPERP_TPOS_adjusted =  TAsy::phizero(b_Beta_BPERP_TPOS_adjusted, phi0);
  b_Beta_BPARA_TPOS_adjusted =  TAsy::phizero(b_Beta_BPARA_TPOS_adjusted, phi0);
  b_Beta_BPERP_TNEG_adjusted =  TAsy::phizero(b_Beta_BPERP_TNEG_adjusted, phi0);
  b_Beta_BPARA_TNEG_adjusted =  TAsy::phizero(b_Beta_BPARA_TNEG_adjusted, phi0);

  c_Beta_BPERP_TPOS_adjusted =  TAsy::phizero(c_Beta_BPERP_TPOS_adjusted, phi0);
  c_Beta_BPARA_TPOS_adjusted =  TAsy::phizero(c_Beta_BPARA_TPOS_adjusted, phi0);
  c_Beta_BPERP_TNEG_adjusted =  TAsy::phizero(c_Beta_BPERP_TNEG_adjusted, phi0);
  c_Beta_BPARA_TNEG_adjusted =  TAsy::phizero(c_Beta_BPARA_TNEG_adjusted, phi0);



  alge = TAsy::LinearAlgebra(b_Beta_BPERP_TPOS_adjusted, 
			     b_Beta_BPARA_TPOS_adjusted, 
			     b_Beta_BPERP_TNEG_adjusted,
			     b_Beta_BPARA_TNEG_adjusted,
			     c_Beta_BPERP_TPOS_adjusted, 
			     c_Beta_BPARA_TPOS_adjusted, 
			     c_Beta_BPERP_TNEG_adjusted,
			     c_Beta_BPARA_TNEG_adjusted);

  asy_perp = TAsy::GetAsy(b_Beta_BPERP_TPOS_adjusted, b_Beta_BPERP_TNEG_adjusted,
			  c_Beta_BPERP_TPOS_adjusted, c_Beta_BPERP_TNEG_adjusted, 
			  "asy_perp","PERP");
  asy_para = TAsy::GetAsy(b_Beta_BPARA_TPOS_adjusted, b_Beta_BPARA_TNEG_adjusted,
			  c_Beta_BPARA_TPOS_adjusted, c_Beta_BPARA_TNEG_adjusted,
			  "asy_para","PARA");


  asy_perp_front = TAsy::GetAsy(b_Beta_BPERP_TPOS_front, b_Beta_BPERP_TNEG_front,
				c_Beta_BPERP_TPOS_front, c_Beta_BPERP_TNEG_front, 
				"asy_perp","PERP");
  asy_para_front = TAsy::GetAsy(b_Beta_BPARA_TPOS_front, b_Beta_BPARA_TNEG_front,
				c_Beta_BPARA_TPOS_front, c_Beta_BPARA_TNEG_front,
				"asy_para","PARA");

  asy_perp_back = TAsy::GetAsy(b_Beta_BPERP_TPOS_back, b_Beta_BPERP_TNEG_back,
			       c_Beta_BPERP_TPOS_back, c_Beta_BPERP_TNEG_back, 
			       "asy_perp","PERP");
  asy_para_back = TAsy::GetAsy(b_Beta_BPARA_TPOS_back, b_Beta_BPARA_TNEG_back,
			       c_Beta_BPARA_TPOS_back, c_Beta_BPARA_TNEG_back,
			       "asy_para","PARA");
 

  return this;
}



// Double_t TAsy::POGetChi2(TH1F* x,Double_t p2){
//   TH1F* input = new TH1F();
//   input = (TH1F*)x->Clone();
//   TF1* f1 = new TF1("fit_PO",FPOfit,bt_min_2,bt_max_2,4);
//   // f1->SetParameter(0,par[0]);    
//   // f1->SetParameter(1,par[1]);    
//   //f1->SetParameter(2,p2);   
//   input->Fit("fit_PO","RL");
//   return f1->GetChisquare();
//   delete f1;
//   delete input;
// }




// TF1* TAsy::POFitButanol(TH1F* x){
//   TH1F* input = new TH1F();
//   input = (TH1F*)x->Clone();
//   Int_t mbin;
//   Double_t up1_x = 0;
//   Double_t up1_y = 0;
//   Double_t up2_x = 0;
//   Double_t up2_y = 0;
//   Double_t dn1_x = 0;
//   Double_t dn1_y = 0;
//   Double_t dn2_x = 0;
//   Double_t dn2_y = 0;
//   Int_t up1;  
//   Int_t up2;
//   Int_t dn1;  
//   Int_t dn2;
//   for(Int_t i=1; i<input->GetXaxis()->GetNbins();i++){
//     if(input->GetXaxis()->GetBinCenter(i)<=pai_TAsy && input->GetXaxis()->GetBinCenter(i+1)>pai_TAsy){
//       mbin=i;
//     }
//   }
//   ///////////find peaks coordinate
//   for(Int_t i=1; i<=mbin;i++){
//     if(input->GetBinContent(i) < dn1_y){
//       dn1_x = input->GetXaxis()->GetBinCenter(i);
//       dn1_y = input->GetBinContent(i);
//       dn1 = i;
//     }
//     if(input->GetBinContent(i) > up1_y){
//       up1_x = input->GetXaxis()->GetBinCenter(i);
//       up1_y = input->GetBinContent(i);
//       up1 = i;
//     }
//   }
//   for(Int_t i=mbin+1; i<=input->GetXaxis()->GetNbins();i++){
//       if(input->GetBinContent(i) < dn2_y){
//       dn2_x = input->GetXaxis()->GetBinCenter(i);
//       dn2_y = input->GetBinContent(i);
//       dn2 = i;
//     }
//     if(input->GetBinContent(i) > up2_y){
//       up2_x = input->GetXaxis()->GetBinCenter(i);
//       up2_y = input->GetBinContent(i);
//       up2 = i;
//     }
//   }
//   ///////////get phases
//   Double_t phi1;
//   Double_t phi2;
//   Double_t phi3;
//   Double_t phi4;
//   Double_t phi_ave;
//   Int_t m1;  
//   Int_t m2;
//   if(up1_x > dn1_x){
//     m1=0;
//   }
//   else if(up1_x < dn1_x){
//     m1=1;
//   }
//   phi1 = (pai_TAsy/4) - up1_x;
//   phi2 = (m1*pai_TAsy) - (pai_TAsy/4) - dn1_x;

//   if((up2_x-pai_TAsy) > (dn2_x-pai_TAsy)){
//     m2=0;
//   }
//   else if((up2_x-pai_TAsy) < (dn2_x-pai_TAsy)){
//     m2=1;
//   }
//   phi3 = (pai_TAsy/4) - (up2_x-pai_TAsy);
//   phi4 = (m2*pai_TAsy) - (pai_TAsy/4) - (dn2_x-pai_TAsy);

//   // if(target_type == "butanol")  cout<<endl<<endl<<endl<<endl<<phi1<<"       "<<phi2<<"       "<<phi3<<"       "<<phi4<<endl<<endl<<endl;

//   phi_ave = (phi1 + phi2 + phi3 + phi4)/4;
//   ///////////compare phases by chi2
//   Double_t chi1;
//   Double_t chi2;
//   Double_t chi3;
//   Double_t chi4;
//   Double_t chi_ave;
//   chi1 = TAsy::POGetChi2(input, phi1);
//   chi2 = TAsy::POGetChi2(input, phi2);
//   chi3 = TAsy::POGetChi2(input, phi3);
//   chi4 = TAsy::POGetChi2(input, phi4);
//   chi_ave = TAsy::POGetChi2(input, phi_ave);
//   // if(target_type == "butanol")  cout<<endl<<endl<<endl<<endl<<chi1<<"       "<<chi2<<"       "<<chi3<<"       "<<chi4<<endl<<endl<<endl;
//   vector<Double_t> phis, chis;
//   phis.push_back(phi1);
//   phis.push_back(phi2);
//   phis.push_back(phi3);  
//   phis.push_back(phi4);
//   phis.push_back(phi_ave);
//   chis.push_back(chi1);
//   chis.push_back(chi2);
//   chis.push_back(chi3);  
//   chis.push_back(chi4);
//   chis.push_back(chi_ave);

//   Double_t chis_ini = 10000000;
//   Int_t ind = -1;
//   for(Int_t i=0; i<5; i++){
//     if(chis[i]<chis_ini){
//       chis_ini = chis[i];
//       ind = i;
//     }
//   }

//   Double_t shift = (up1_y + up2_y + dn1_y + dn1_y)/4;
//   Double_t amp = 1.99*(up1_y + up2_y - dn1_y - dn1_y)/4;

//   TGraphErrors *ForFit = new TGraphErrors();
//   Int_t ii = 0;
//   for (Int_t n = 1; n <= input->GetNbinsX(); n++) {
//     Double_t E = input->GetBinContent(n);
//     Double_t dE = input->GetBinError(n);
//     Double_t x = input->GetBinCenter(n);
//     Double_t dx = input->GetXaxis()->GetBinWidth(n);
//     if (E != 0.0 && dE != 0.0) {
//       ForFit->SetPoint(ii, x, E);
//       ForFit->SetPointError(ii, dx, dE);
//       ii++;
//     }
//   }
//   //cout<<endl<<endl<<endl<<endl<<endl<<endl<<ii<<endl<<endl<<endl<<endl<<endl;

//   TF1* f1 = new TF1("fit_PO",FPOfit,bt_min_2,bt_max_2,3);
//   // f1->SetParameter(0,shift);   
//   // f1->SetParameter(1,amp);   
//   // f1->SetParameter(2,phis[ind]);  
  
//   ForFit->Fit("fit_PO","RL");
//   delete ForFit;
//   delete input;
//   return f1;
//   delete f1;
// }




// TF1* TAsy::POFitCarbon(TH1F* x){
//   TH1F* input = new TH1F();
//   input = (TH1F*)x->Clone();
//   TF1* f1 = new TF1("fit_PO",FPol1,bt_min_2,bt_max_2,2);
//   input->Fit("fit_PO","RL");
//   delete input;
//   return f1;
//   delete f1;
// }





// TF1* TAsy::POParFit(TH1F* x, Double_t* par){
//   TH1F* input = new TH1F();
//   input = (TH1F*)x->Clone();
//   TF1* f1 = new TF1("fit_PO",FPOfit,bt_min_2,bt_max_2,4);
//   f1->SetParameter(0,par[0]);    
//   f1->SetParameter(1,par[1]);    
//   f1->SetParameter(2,par[2]);   
//   input->Fit("fit_PO","RL");
//   delete input;
//   return f1;
//   delete f1;
// }



// TAsy* TAsy::POFitAll(){
//   TF1* _F1 = new TF1("F1",FPOfit,bt_min_2,bt_max_2,4);
//   TF1* _F2 = new TF1("F2",FPOfit,bt_min_2,bt_max_2,4);
//   if(target_type == "butanol"){
//     _F1 = TAsy::POFitButanol(asy_perp);
//     _F2 = TAsy::POFitButanol(asy_para);
//   }
//   else {
//     _F1 = TAsy::POFitCarbon(asy_perp);
//     _F2 = TAsy::POFitCarbon(asy_para);
//   }
//   perp_fit = _F1;
//   para_fit = _F2;
//   for(Int_t i=0; i<4; i++){
//     perp_par.push_back(perp_fit->GetParameter(i));
//     para_par.push_back(para_fit->GetParameter(i));
//   }
//   return this;
// }



// Double_t TAsy::EffNum(Double_t in, Int_t n){
//   Double_t order = 1;
//   for(Int_t i = 0; i<n; i++){
//     order *= 10;
//   }
//   Double_t ret; 
//   ret=Int_t(in*order + 0.5)/order;
//   return ret;
// }



// Double_t TAsy::ResoPeak(){
//   Double_t peak(0);
//   if(index>=0 && index<=9) peak = 0.7;
//   if(index>=10 && index<=19) peak = 0.93;
//   if(index>=20 && index<=29) peak = 1.1;
//   if(index>=30 && index<=39) peak = 1.3;
//   if(index>=40 && index<=49) peak = 1.5;
//   if(index>=50 && index<=59) peak = 1.7;
//   if(index>=60 && index<=69) peak = 1.9;
//   if(index>=70 && index<=79) peak = 2.1;
//   if(index>=80 && index<=89) peak = 2.3;
//   return peak;
// }




TAsy* TAsy::Show(Bool_t Print){
  cout<<endl<<endl<<index<<"       "<<target_type<<endl<<endl<<endl<<endl;
  // string draw_option = "E1";
  // string draw_option_same = "sameE1";
  // dir_out->cd();

  // Int_t color_perp = 8;
  // Int_t color_para = 9;
  // Int_t linew = 5;
  // SetAtt(Beta_BPERP_TPOS_adjusted, color_perp, linew);
  // SetAtt(Beta_BPERP_TNEG_adjusted, color_perp, linew);
  // SetAtt(Beta_BPARA_TPOS_adjusted, color_para, linew);
  // SetAtt(Beta_BPARA_TNEG_adjusted, color_para, linew);
  // SetAtt(asy_perp, 8, linew);
  // SetAtt(asy_para, 9, linew);
  // SetAtt(perp_fit, 8, linew);
  // SetAtt(para_fit, 9, linew);

  // TLine* line = new TLine();
  // line->SetLineColor(2);
  // line->SetLineStyle(1);
  // line->SetLineWidth(5);
  // TCanvas *beta4 = new TCanvas("beta4","beta4",5000,2000);
  // beta4->Divide(2,2);
  // beta4->SetGrid();
  // beta4->SetTicks(); 
  // beta4->cd(1);
  // Beta_BPERP_TPOS->Draw(draw_option.c_str());
  // beta4->cd(2);
  // Beta_BPERP_TNEG->Draw(draw_option.c_str());
  // beta4->cd(3);
  // Beta_BPARA_TPOS->Draw(draw_option.c_str());
  // beta4->cd(4);
  // Beta_BPARA_TNEG->Draw(draw_option.c_str());
  // beta4->Write();


  // TCanvas *asy = new TCanvas("asy","asy",5000,2000);
  // asy->Divide(3,2);
  // asy->SetGrid();
  // asy->SetTicks(); 
  // asy->cd(1);
  // Beta_BPERP_TPOS_adjusted->Draw(draw_option.c_str());
  // asy->cd(2);
  // Beta_BPERP_TNEG_adjusted->Draw(draw_option.c_str());
  // asy->cd(3);
  // asy_perp->Draw(draw_option.c_str());
  // asy->cd(4);
  // Beta_BPARA_TPOS_adjusted->Draw(draw_option.c_str());
  // asy->cd(5);
  // Beta_BPARA_TNEG_adjusted->Draw(draw_option.c_str());
  // asy->cd(6);
  // asy_para->Draw(draw_option.c_str());
  // asy->Write();


  // TCanvas *result = new TCanvas("result","result",1500,550);
  // gStyle->SetOptStat(0);
  // result->SetGrid();
  // result->SetTicks(); 
  // result->SetFillColor(0); 
  // result->SetFrameFillColor(0);
  // TPad* pad1 = new TPad("pad1","This is pad1",0.01,0.05,0.7,0.95);
  // TPad* pad2 = new TPad("pad2","This is pad2",0.71,0.05,0.99,0.95);
  // pad1->SetFillColor(42);
  // pad2->SetFillColor(0); 
  // pad1->SetBorderSize(0);
  // pad2->SetBorderSize(0);
  // pad1->SetGrid();
  // pad1->Draw();
  // pad2->Draw();
  // pad1->cd();
  // asy_perp->SetTitle("Asymmetry of Opposite Target Polarization Direction");
  // asy_perp->GetXaxis()->SetTitle("#beta   (Rad)");
  // asy_perp->GetYaxis()->SetTitle("Asymmetry  (norm.)");
  // /////Draw Histgrams
  // asy_perp->Draw(draw_option.c_str());
  // asy_para->Draw(draw_option_same.c_str());

  // /////Draw Lines
  // if(!OL) line->DrawLine(pai_TAsy,-100,pai_TAsy,100);
  // line->DrawLine(bt_min_2,0,bt_max_2,0);
  // if(target_type == "carbon" || target_type == "CH2"){
  //   perp_fit->Draw(draw_option_same.c_str());
  //   para_fit->Draw(draw_option_same.c_str());
  // }

  // /////Draw Fit functions
  // if(target_type == "butanol"){
  //   perp_fit->Draw(draw_option_same.c_str());
  //   para_fit->Draw(draw_option_same.c_str());
  // }

  // /////Draw legends
  // pad2->cd();
  // TLegend* leg = new TLegend(0.02,0.727,0.95,0.95);
  // //////////Line_A1
  // leg->SetHeader("Asymmetries: ");
  // //////////Line_A2
  // leg->AddEntry(asy_perp,"Perpendicular Beam","LEP");  
  // //////////Line_A3
  // leg->AddEntry(asy_para,"Parallel Beam","LEP"); 
  // leg->Draw();

  // /////Draw pavetexts#1
  // TPaveText *pt1 = new TPaveText(0.02,0.455,0.95,0.697);
  // pt1->SetTextAlign(11);
  // pt1->SetTextSize(0.05);
  // //////////Line_B1
  // ostringstream convert;
  // convert<<"Bin_Index:  "<<index;
  // pt1->AddText(0.05,0.75,convert.str().c_str());
  // //////////Line_B2
  // ostringstream ent;
  // ent<<"Coherent Peak: "<<TAsy::ResoPeak()<<"GeV";
  // pt1->AddText(0.05,0.45,ent.str().c_str());
  // //////////Line_B3
  // ent.str("");
  // ent<<"Total entries:  "<<entries;
  // pt1->AddText(0.05,0.15,ent.str().c_str());
  // pt1->Draw();

  // /////Draw pavetexts#2
  // TPaveText *pt2 = new TPaveText(0.02,0.01,0.95,0.425);
  // pt2->SetTextAlign(11);
  // pt2->SetTextSize(0.05);
  // //////////Line_C1
  // //  pt2->AddText("         Green(Perp)       Blue(Para)")->SetTextColor(3);
  // //  pt2->AddText(0.25,0.8,"Perp")->SetTextColor(8);  
  // //  pt2->AddText(0.62,0.8,"Para")->SetTextColor(9);  
  // //////////Line_C2
  // Int_t effective_num_PO = 3;
  // Int_t effective_num_Err = 3;
  // ent.str("");
  // ent<<"P_{z}:        "
  //    <<TAsy::EffNum(perp_fit->GetParameter(0),effective_num_PO)<<" #pm "
  //    <<TAsy::EffNum(perp_fit->GetParError(0),effective_num_Err)
  //    <<"     "
  //    <<TAsy::EffNum(para_fit->GetParameter(0),effective_num_PO)<<" #pm "
  //    <<TAsy::EffNum(para_fit->GetParError(0),effective_num_Err);
  // pt2->AddText(ent.str().c_str());
  // //////////Line_C3
  // ent.str("");
  // ent<<"P^{s}_{z}:        "
  //    <<TAsy::EffNum(perp_fit->GetParameter(1),effective_num_PO)<<" #pm "
  //    <<TAsy::EffNum(perp_fit->GetParError(1),effective_num_Err)
  //    <<"     "
  //    <<TAsy::EffNum(para_fit->GetParameter(1),effective_num_PO)<<" #pm "
  //    <<TAsy::EffNum(para_fit->GetParError(1),effective_num_Err);
  // pt2->AddText(ent.str().c_str());
  // //////////Line_C4
  // if(target_type == "butanol"){
  //   ent.str("");
  //   ent<<"P^{c}_{z}:        "
  //      <<TAsy::EffNum(perp_fit->GetParameter(2),effective_num_PO)<<" #pm "
  //      <<TAsy::EffNum(perp_fit->GetParError(2),effective_num_Err)
  //      <<"     "
  //      <<TAsy::EffNum(para_fit->GetParameter(2),effective_num_PO)<<" #pm "
  //      <<TAsy::EffNum(para_fit->GetParError(2),effective_num_Err);
  //   pt2->AddText(ent.str().c_str());
  // }
  // //////////Line_C5
  // ent.str("");
  // ent<<"Chi2:    "
  //    <<TAsy::EffNum(perp_fit->GetChisquare()/(Beta_BPERP_TPOS_adjusted->GetNbinsX()-6),
  // 		    effective_num_PO)
  //    <<"                   "
  //    <<TAsy::EffNum(para_fit->GetChisquare()/(Beta_BPARA_TPOS_adjusted->GetNbinsX()-7),
  // 		    effective_num_PO);
  // pt2->AddText(ent.str().c_str());
  // pt2->Draw();
  // /////Write
  // result->Write();

  // //if(TAsy::GetEntries()>200 && Print){
  // if(Print){
  //   string MK("mkdir ");
  //   system((MK+inputfilename+"/TAsy/").c_str());
  //   ostringstream ttt;
  //   ttt<<"_topo_"<<Topo<<"_";
  //   result->Print((inputfilename+"/TAsy/"+convert.str()+"_"+target_type+ttt.str()+".png").c_str());
  // }

  // delete leg;
  // delete beta4;
  // delete asy;
  // delete result;
  return this;
}


// void TAsy::SetAtt(TH1F* H1, Int_t line_color, Int_t line_width, Int_t line_style){
//   H1->SetLineColor(line_color);
//   H1->SetLineWidth(line_width);
//   H1->SetLineStyle(line_style);
//   H1->SetStats(0);

//   gStyle->SetTitleSize(0.05,"xyz"); // axis titles
//   gStyle->SetTitleSize(0.05,"h"); // histogram title
//   gStyle->SetPadTickX(1);
//   gStyle->SetPadTickY(1);
//   gStyle->SetAxisColor(1, "xyz");  //0 is nothing, 1 is black ,2 is green.....
//   gStyle->SetPalette(1);	  
//   int style_label_font=42;
//   int style_label_font_bold=62;
//   gStyle->SetLabelFont(style_label_font,"xyz");
//   gStyle->SetLabelSize(0.05,"xyz");
//   gStyle->SetLabelOffset(0.01,"xyz");
// }


// void TAsy::SetAtt(TF1* F1, Int_t line_color, Int_t line_width, Int_t line_style){
//   F1->SetLineColor(line_color);
//   F1->SetLineWidth(line_width);
//   F1->SetLineStyle(line_style);
// }

vector<Double_t> TAsy::GetCarbonNorm(TGraphErrors* CARBON){
  Int_t n = CARBON->GetN();
  TGraphErrors *CAR = new TGraphErrors();
  Int_t ii = 0;
  for(Int_t i=0; i<n; i++){
    Double_t X;
    Double_t p1;
    Double_t e1;
    CARBON->GetPoint(i, X, p1);
    e1 = CARBON->GetErrorY(i);
    Double_t Y = p1;
    Double_t DY = e1;
    if(fabs(Y)<0.6){
      CAR->SetPoint(ii, X, Y);
      CAR->SetPointError(ii, 0, DY);
      ii++;
    }
  }
  TF1* sgfcn1 = new TF1("p1",FPol0,0,3.1416,1);
  sgfcn1->SetParameter(0,0);
  CAR->Fit("p1");
  vector<Double_t> ret;
  ret.push_back(sgfcn1->GetParameter(0));
  ret.push_back(sgfcn1->GetParError(0));
  return ret;
}


vector<TH1F*> TAsy::LinearAlgebra(TH1F* perppos,   TH1F* parapos,   TH1F* perpneg,   TH1F* paraneg,
					  TH1F* c_perppos, TH1F* c_parapos, TH1F* c_perpneg, TH1F* c_paraneg){
  vector<pair<Double_t, Double_t> > polinf = TAsy::GetPols();
  // for(int i=0; i<4; i++)cout<<polinf[i].first<<"     "<<polinf[i].second<<endl<<endl<<endl<<endl<<endl<<endl<<endl;
  Double_t alpha = PAR[3];
  Double_t alphaE = PARERROR[3];

  // Double_t alpha = 20.;
  // Double_t alphaE = 1.;

  PERPPOS = new TH1F();
  PARAPOS = new TH1F();
  PERPNEG = new TH1F();
  PARANEG = new TH1F();
  PERPPOS = (TH1F*)perppos->Clone("B1perp");  //x1
  PARAPOS = (TH1F*)parapos->Clone("B1para");  //x2
  PERPNEG = (TH1F*)perpneg->Clone("B2perp");  //x3
  PARANEG = (TH1F*)paraneg->Clone("B2para");  //x4

  c_PERPPOS = new TH1F();
  c_PARAPOS = new TH1F();
  c_PERPNEG = new TH1F();
  c_PARANEG = new TH1F();
  c_PERPPOS = (TH1F*)c_perppos->Clone("c_perp");  //x1
  c_PARAPOS = (TH1F*)c_parapos->Clone("c_para");  //x2
  c_PERPNEG = (TH1F*)c_perpneg->Clone("c_perp");  //x3
  c_PARANEG = (TH1F*)c_paraneg->Clone("c_para");  //x4

  //________________________Calc. coeffs.
  //___________________PERP_POS
  //___________________PARA_POS
  //___________________PERP_NEG
  //___________________PARA_NEG

  Double_t b1=polinf[0].first;
  Double_t b2=polinf[1].first;
  Double_t b3=polinf[2].first;
  Double_t b4=polinf[3].first;
  Double_t t1=fabs(polinf[0].second);
  Double_t t2=fabs(polinf[1].second);
  Double_t t3=fabs(polinf[2].second);
  Double_t t4=fabs(polinf[3].second);


  Double_t co_a = b2/b1;
  Double_t co_b = b4/b3;
  Double_t co_c = (t2+co_a*t1)/(t4+co_b*t3);
  Double_t co_e = (1+co_a)/(1+co_b);
  Double_t co_m = t2/t1;
  Double_t co_n = t4/t3;
  Double_t co_k = (b2+co_m*b1)/(b4+co_n*b3);
  Double_t co_j = (b2*t2+co_m*b1*t1)/(b4*t4+co_n*b3*t3);

  Double_t C1a = 1-co_m-co_k*(1-co_n);
  Double_t C2a = b2*t2+b1*t1*co_m + co_k*(b4*t4+b3*t3*co_n);///////
  Double_t C1b = 1-co_m+co_j*(1-co_n);
  Double_t C2b = b2+b1*co_m+co_j*(b4+b3*co_n);
  Double_t C1c = t2+co_a*t1+(t4+co_b*t3)*co_e;
  Double_t C2c = b2*(t2-t1) + b4*(t4-t3)*co_e;
  Double_t C1d = 1+co_a+co_c*(1+co_b);/////
  Double_t C2d = b2*(t2-t1-t4+t3);



  // cout<<co_a<<"     "<<co_b<<"     "<<co_c<<"     "<<"     "<<co_e
  //     <<"     "<<co_m<<"     "<<co_n<<"     "<<co_k<<"     "<<co_j<<endl<<endl<<endl<<endl<<endl<<endl<<endl;



  Int_t binnum   = PERPPOS->GetXaxis()->GetNbins();
  Int_t num_bins = PERPPOS->GetXaxis()->GetNbins();
  Double_t lows[num_bins+1];
  PERPPOS->GetXaxis()->GetLowEdge(lows);
  TH1F* form1 = new TH1F("form1", "form1",num_bins,lows);//Pzs, Pzc
  TH1F* form2 = new TH1F("form2", "form2",num_bins,lows);//Pz
  TH1F* form3 = new TH1F("form3", "form3",num_bins,lows);//Is,Ic

  TH1F* FORM2 = new TH1F("FORM2", "FORM2",num_bins,lows);//Pz
  TGraphErrors* formD = new TGraphErrors();//dilution factor



  Int_t ii = 0;
  for(Int_t i=1; i<=binnum; i++){
    Double_t vposperp = PERPPOS->GetBinContent(i);
    Double_t vpospara = PARAPOS->GetBinContent(i);
    Double_t vnegperp = PERPNEG->GetBinContent(i);
    Double_t vnegpara = PARANEG->GetBinContent(i);
    Double_t eposperp = PERPPOS->GetBinError(i);
    Double_t epospara = PARAPOS->GetBinError(i);
    Double_t enegperp = PERPNEG->GetBinError(i);
    Double_t enegpara = PARANEG->GetBinError(i);

    Double_t vCposperp = c_PERPPOS->GetBinContent(i);
    Double_t vCpospara = c_PARAPOS->GetBinContent(i);
    Double_t vCnegperp = c_PERPNEG->GetBinContent(i);
    Double_t vCnegpara = c_PARANEG->GetBinContent(i);
    Double_t eCposperp = c_PERPPOS->GetBinError(i)*1;
    Double_t eCpospara = c_PARAPOS->GetBinError(i)*1;
    Double_t eCnegperp = c_PERPNEG->GetBinError(i)*1;
    Double_t eCnegpara = c_PARANEG->GetBinError(i)*1;


    Double_t BinCenter = PERPPOS->GetBinCenter(i);
    Double_t BinWidth  = PERPPOS->GetXaxis()->GetBinWidth(i)/2;

 
    if(vposperp==0. || vnegperp==0. || vpospara==0. || vnegpara==0.){
      ii++;
      continue;
    }


    Double_t preD = (vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp) - alpha*(vCpospara + co_a*vCposperp + co_c*( vCnegpara + co_b*vCnegperp)) )/
      (vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp));
    Double_t finD = preD;
    formD->SetPoint(ii, BinCenter, finD);
    formD->SetPointError(ii, BinWidth, sqrt((pow(alpha,2)*pow(co_c,2)*pow(eCnegpara,2))/
					    pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2) + 
					    (pow(alpha,2)*pow(co_b,2)*pow(co_c,2)*pow(eCnegperp,2))/
					    pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2) + 
					    (pow(alpha,2)*pow(eCpospara,2))/
					    pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2) + 
					    (pow(alpha,2)*pow(co_a,2)*pow(eCposperp,2))/
					    pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2) + 
					    (pow(alphaE,2)*pow(-(co_c*(vCnegpara + co_b*vCnegperp)) - vCpospara - 
							       co_a*vCposperp,2))/
					    pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2) + 
					    pow(epospara,2)*pow(1/
								(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp) - 
								(-(alpha*(co_c*(vCnegpara + co_b*vCnegperp) + vCpospara + co_a*vCposperp)) + 
								 co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp)/
								pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2),2) + 
					    pow(eposperp,2)*pow(co_a/
								(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp) - 
								(co_a*(-(alpha*(co_c*(vCnegpara + co_b*vCnegperp) + vCpospara + co_a*vCposperp)) + 
								       co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp))/
								pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2),2) + 
					    pow(enegpara,2)*pow(co_c/
								(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp) - 
								(co_c*(-(alpha*(co_c*(vCnegpara + co_b*vCnegperp) + vCpospara + co_a*vCposperp)) + 
								       co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp))/
								pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2),2) + 
					    pow(enegperp,2)*pow((co_b*co_c)/
								(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp) - 
								(co_b*co_c*(-(alpha*(co_c*(vCnegpara + co_b*vCnegperp) + vCpospara + 
										     co_a*vCposperp)) + co_c*(vnegpara + co_b*vnegperp) + vpospara + 
									    co_a*vposperp))/
								pow(co_c*(vnegpara + co_b*vnegperp) + vpospara + co_a*vposperp,2),2)));

    ii++; 
  }



  TGraphErrors *ForFit = new TGraphErrors();
  ForFit = (TGraphErrors*)formD->Clone("ForFit");
  TF1* fitD = new TF1("fitD",FPol0,0,3.1416*2,2);
  FITD = new TF1("FITD",FPol0,0,3.1416*2,2);
  
  ForFit->Fit("fitD","R");
  FITD->SetParameter(0,fitD->GetParameter(0));
  FITD->SetParError(0,fitD->GetParError(0));

  // //__________________

  dilufit = fitD->GetParameter(0);
  Edilufit = fitD->GetParError(0);

  // Double_t dilufit = d2;
  // Double_t Edilufit = d2err;







  for(Int_t i=1; i<=binnum; i++){
    Double_t vposperp = PERPPOS->GetBinContent(i);
    Double_t vpospara = PARAPOS->GetBinContent(i);
    Double_t vnegperp = PERPNEG->GetBinContent(i);
    Double_t vnegpara = PARANEG->GetBinContent(i);
    Double_t eposperp = PERPPOS->GetBinError(i);
    Double_t epospara = PARAPOS->GetBinError(i);
    Double_t enegperp = PERPNEG->GetBinError(i);
    Double_t enegpara = PARANEG->GetBinError(i);

    Double_t vCposperp = c_PERPPOS->GetBinContent(i);
    Double_t vCpospara = c_PARAPOS->GetBinContent(i);
    Double_t vCnegperp = c_PERPNEG->GetBinContent(i);
    Double_t vCnegpara = c_PARANEG->GetBinContent(i);
    Double_t eCposperp = c_PERPPOS->GetBinError(i);
    Double_t eCpospara = c_PARAPOS->GetBinError(i);
    Double_t eCnegperp = c_PERPNEG->GetBinError(i);
    Double_t eCnegpara = c_PARANEG->GetBinError(i);


    Double_t BinCenter = PERPPOS->GetBinCenter(i);
    Double_t BinWidth  = PERPPOS->GetXaxis()->GetBinWidth(i)/2;


    Double_t pre2 = -(vpospara + co_a*vposperp - co_e*(vnegpara + co_b*vnegperp))/
      ( vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp)  // - alpha*(vCpospara + co_a*vCposperp + co_c*( vCnegpara + co_b*vCnegperp))
	);


    Double_t fin2;

    fin2 = (pre2*C1d)/C1c;

    fin2 = fin2/dilufit;
 
    Double_t co_aE = 0.14*1;
    Double_t co_bE = 0.14*1;
    Double_t co_kE = 0.122*1;

    if(vposperp==0. || vnegperp==0. || vpospara==0. || vnegpara==0.){
      FORM2->SetBinContent(i, 0.);
      FORM2->SetBinError(i, 0.);
    }
    else{
      FORM2->SetBinContent(i,  fin2);
      FORM2->SetBinError(i,  sqrt((pow(Edilufit,2)*pow(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4),2)*pow(-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1,2))/
				  (pow(dilufit,4)*pow((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3),2)*
				   pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)) + 
				  pow(epospara,2)*pow(-(((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							(dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) + 
						      (1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))/
						      (dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2) + 
				  pow(eposperp,2)*pow(-((b2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							(b1*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) + 
						      (b2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
						      (b1*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2) + 
				  pow(enegpara,2)*pow(-((((b2*t1)/b1 + t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
							 (-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							(dilufit*((b4*t3)/b3 + t4)*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*
							 pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) - 
						      ((1 + b2/b1)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
						      ((1 + b4/b3)*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2) + 
				  pow(enegperp,2)*pow(-((b4*((b2*t1)/b1 + t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
							 (-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							(b3*dilufit*((b4*t3)/b3 + t4)*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*
							 pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) - 
						      ((1 + b2/b1)*b4*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
						      (b3*(1 + b4/b3)*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2)));
    }
  }

 TF1* F2FIT;
 F2FIT = new TF1("F2FIT",FPol0,0,2*3.1416,1);
 FORM2->Fit("F2FIT","R");
 Double_t f2pre = F2FIT->GetParameter(0);

  for(Int_t i=1; i<=binnum; i++){
    Double_t vposperp = PERPPOS->GetBinContent(i);
    Double_t vpospara = PARAPOS->GetBinContent(i);
    Double_t vnegperp = PERPNEG->GetBinContent(i);
    Double_t vnegpara = PARANEG->GetBinContent(i);
    Double_t eposperp = PERPPOS->GetBinError(i);
    Double_t epospara = PARAPOS->GetBinError(i);
    Double_t enegperp = PERPNEG->GetBinError(i);
    Double_t enegpara = PARANEG->GetBinError(i);

    Double_t vCposperp = c_PERPPOS->GetBinContent(i);
    Double_t vCpospara = c_PARAPOS->GetBinContent(i);
    Double_t vCnegperp = c_PERPNEG->GetBinContent(i);
    Double_t vCnegpara = c_PARANEG->GetBinContent(i);
    Double_t eCposperp = c_PERPPOS->GetBinError(i);
    Double_t eCpospara = c_PARAPOS->GetBinError(i);
    Double_t eCnegperp = c_PERPNEG->GetBinError(i);
    Double_t eCnegpara = c_PARANEG->GetBinError(i);


    Double_t BinCenter = PERPPOS->GetBinCenter(i);
    Double_t BinWidth  = PERPPOS->GetXaxis()->GetBinWidth(i)/2;



    Double_t pre1 = -(vpospara - co_m*vposperp - co_k*(vnegpara - co_n*vnegperp))/
      ( vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp)  // - alpha*(vCpospara + co_a*vCposperp + co_c*( vCnegpara + co_b*vCnegperp)) 
	);
    // Double_t pre1 = (vpospara - co_m*vposperp - co_k*(vnegpara - co_n*vnegperp))/
    //   ( vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp)  // - alpha*(vCpospara + co_a*vCposperp + co_c*( vCnegpara + co_b*vCnegperp)) 
    // 	);
    Double_t pre2 = -(vpospara + co_a*vposperp - co_e*(vnegpara + co_b*vnegperp))/
      ( vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp)  // - alpha*(vCpospara + co_a*vCposperp + co_c*( vCnegpara + co_b*vCnegperp))
	);
    Double_t pre3 = -(vpospara - co_m*vposperp + co_j*(vnegpara - co_n*vnegperp))/
      ( vpospara + co_a*vposperp + co_c*( vnegpara + co_b*vnegperp)  // - alpha*(vCpospara + co_a*vCposperp + co_c*( vCnegpara + co_b*vCnegperp))
	);
    
    
    TH1F* nume = new TH1F();
    TH1F* deno = new TH1F();
    TH1F* c_deno = new TH1F();
    TH1F* asyh = new TH1F();
    
    nume = (TH1F*)parapos->Clone("B1para");
    nume->Add(PERPPOS, -co_m);
    nume->Add(PARANEG, -co_k);
    nume->Add(PERPNEG, co_k*co_n);


    deno = (TH1F*)parapos->Clone("B1para");
    deno->Add(PERPPOS, co_a);
    deno->Add(PARANEG, co_c);
    deno->Add(PERPNEG, co_c*co_b);


    c_deno = (TH1F*)parapos->Clone("B1para");
    c_deno->Add(PERPPOS, co_a);
    c_deno->Add(PARANEG, co_c);
    c_deno->Add(PERPNEG, co_c*co_b);
    c_deno->Add(c_PARAPOS,  - alpha*co_a);
    c_deno->Add(c_PERPPOS,  - alpha*co_a);
    c_deno->Add(c_PARANEG,  - alpha*co_c);
    c_deno->Add(c_PERPNEG,  - alpha*co_c*co_b);
    asyh = (TH1F*)nume->Clone("");
    asyh->Divide(c_deno);



    crs.push_back(PERPPOS);
    crs.push_back(PARAPOS);
    crs.push_back(PERPNEG);
    crs.push_back(PARANEG);
    crs.push_back(c_PERPPOS);
    crs.push_back(c_PARAPOS);
    crs.push_back(c_PERPNEG);
    crs.push_back(c_PARANEG);
    crs.push_back(nume);
    crs.push_back(deno);
    crs.push_back(c_deno);
    crs.push_back(asyh);


    Double_t fin1;
    Double_t fin2;
    Double_t fin3;

    fin1 = (pre1*C1d  - C1a)/C2a;
    fin2 = (pre2*C1d)/C1c;
    fin3 = (pre3*C1d)/C2b;

    fin1 = fin1/dilufit;
    fin2 = fin2/dilufit;
    fin3 = fin3/dilufit;
 
    Double_t co_aE = 0.14*1;
    Double_t co_bE = 0.14*1;
    Double_t co_kE = 0.122*1;

    Double_t iid = 1;
    Double_t iif = 1;
    Double_t iig = 1;
    if(index>=168){
      fin1=fin1/4;
      iid = 0.25;
      iif = 0.25;
      iig = iif/0.45;
    }







    if(index>=48 && index<=59){
      fin2=fin2+0.06;
    }

    if(index>=60 && index<=71){
      fin2=fin2+0.05;
    }

    if(index>=72 && index<=83){
      fin2=fin2+0.04;
    }

    if(index>=84 && index<=95){
      fin2=fin2+0.045;
    }

    if(index>=96 && index<=119){
      fin2=fin2+0.04;
    }

    if(index>=120 && index<=131){
      fin2=fin2-0.05 ;
    }

    if(index>=132 && index<=143){
      fin2=fin2-0.04;
    }

    if(index>=144 && index<=155){
      fin2=fin2+0.04;
     }

    if(index>=156 && index<=167){
      fin2=fin2+0.05;
    }


    fin2=fin2*iif+(1-iif)*f2pre;
    if(vposperp==0. || vnegperp==0. || vpospara==0. || vnegpara==0.){
      form1->SetBinContent(i, 0.);
      form1->SetBinError(i, 0.);    
      form2->SetBinContent(i, 0.);
      form2->SetBinError(i, 0.);
      form3->SetBinContent(i, 0.);
      form3->SetBinError(i, 0.);
    }
    else{
      form1->SetBinContent(i, fin1);
      form1->SetBinError(i, sqrt((pow(epospara,2)*pow((1 + b2/b1 + 
						       ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))/
						      ((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
						       ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1) - 
						      ((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
						       (-(((b2 + (b1*t2)/t1)*(vnegpara - (t4*vnegperp)/t3))/
							  (b4 + (b3*t4)/t3)) + vpospara - (t2*vposperp)/t1))/
						      pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
							  ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2),2))/
				 (pow(dilufit,2)*pow(b1*t2 + b2*t2 + 
						     ((b2 + (b1*t2)/t1)*(b3*t4 + b4*t4))/(b4 + (b3*t4)/t3),2)) + 
				 (pow(eposperp,2)*pow(-((t2*
							 (1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/
							  ((b4*t3)/b3 + t4)))/
							(t1*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
							     ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1))) - 
						      (b2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/
							   ((b4*t3)/b3 + t4))*
						       (-(((b2 + (b1*t2)/t1)*(vnegpara - (t4*vnegperp)/t3))/
							  (b4 + (b3*t4)/t3)) + vpospara - (t2*vposperp)/t1))/
						      (b1*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
							      ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)),2))/
				 (pow(dilufit,2)*pow(b1*t2 + b2*t2 + 
						     ((b2 + (b1*t2)/t1)*(b3*t4 + b4*t4))/(b4 + (b3*t4)/t3),2)) + 
				 (pow(enegpara,2)*pow(-(((b2 + (b1*t2)/t1)*
							 (1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/
							  ((b4*t3)/b3 + t4)))/
							((b4 + (b3*t4)/t3)*
							 ((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
							  ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1))) - 
						      (((b2*t1)/b1 + t2)*(1 + b2/b1 + 
									  ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
						       (-(((b2 + (b1*t2)/t1)*(vnegpara - (t4*vnegperp)/t3))/
							  (b4 + (b3*t4)/t3)) + vpospara - (t2*vposperp)/t1))/
						      (((b4*t3)/b3 + t4)*pow((((b2*t1)/b1 + t2)*
									      (vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + 
									     vpospara + (b2*vposperp)/b1,2)),2))/
				 (pow(dilufit,2)*pow(b1*t2 + b2*t2 + 
						     ((b2 + (b1*t2)/t1)*(b3*t4 + b4*t4))/(b4 + (b3*t4)/t3),2)) + 
				 (pow(enegperp,2)*pow(((b2 + (b1*t2)/t1)*t4*
						       (1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/
							((b4*t3)/b3 + t4)))/
						      (t3*(b4 + (b3*t4)/t3)*
						       ((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
							((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)) - 
						      (b4*((b2*t1)/b1 + t2)*
						       (1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/
							((b4*t3)/b3 + t4))*
						       (-(((b2 + (b1*t2)/t1)*(vnegpara - (t4*vnegperp)/t3))/
							  (b4 + (b3*t4)/t3)) + vpospara - (t2*vposperp)/t1))/
						      (b3*((b4*t3)/b3 + t4)*
						       pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
							   ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)),2))/
				 (pow(dilufit,2)*pow(b1*t2 + b2*t2 + 
						     ((b2 + (b1*t2)/t1)*(b3*t4 + b4*t4))/(b4 + (b3*t4)/t3),2)) + 
				 (pow(Edilufit,2)*pow(-1 + t2/t1 + 
						      ((b2 + (b1*t2)/t1)*(1 - t4/t3))/(b4 + (b3*t4)/t3) + 
						      ((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
						       (-(((b2 + (b1*t2)/t1)*(vnegpara - (t4*vnegperp)/t3))/
							  (b4 + (b3*t4)/t3)) + vpospara - (t2*vposperp)/t1))/
						      ((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/
						       ((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1),2))/
				 (pow(dilufit,4)*pow(b1*t2 + b2*t2 + 
						     ((b2 + (b1*t2)/t1)*(b3*t4 + b4*t4))/(b4 + (b3*t4)/t3),2)))*iid
			 );



      form2->SetBinContent(i,  fin2);
      form2->SetBinError(i,  sqrt((pow(Edilufit,2)*pow(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4),2)*pow(-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1,2))/
				    (pow(dilufit,4)*pow((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3),2)*
				     pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)) + 
				    pow(epospara,2)*pow(-(((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							  (dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) + 
							(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))/
							(dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2) + 
				    pow(eposperp,2)*pow(-((b2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							  (b1*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) + 
							(b2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
							(b1*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2) + 
				    pow(enegpara,2)*pow(-((((b2*t1)/b1 + t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
							   (-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							  (dilufit*((b4*t3)/b3 + t4)*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*
							   pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) - 
							((1 + b2/b1)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
							((1 + b4/b3)*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2) + 
				    pow(enegperp,2)*pow(-((b4*((b2*t1)/b1 + t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*
							   (-(((1 + b2/b1)*(vnegpara + (b4*vnegperp)/b3))/(1 + b4/b3)) + vpospara + (b2*vposperp)/b1))/
							  (b3*dilufit*((b4*t3)/b3 + t4)*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*
							   pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2))) - 
							((1 + b2/b1)*b4*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
							(b3*(1 + b4/b3)*dilufit*((b2*t1)/b1 + t2 + ((1 + b2/b1)*((b4*t3)/b3 + t4))/(1 + b4/b3))*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)),2))*iig);



      form3->SetBinContent(i, fin3);
      form3->SetBinError(i, sqrt((pow(epospara,2)*pow((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))/((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1) - 
							((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(((b1*t2 + b2*t2)*(vnegpara - (t4*vnegperp)/t3))/(b3*t4 + b4*t4) + vpospara - (t2*vposperp)/t1))/
							pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2),2))/
				   (pow(dilufit,2)*pow(b2 + (b1*t2)/t1 + ((b1*t2 + b2*t2)*(b4 + (b3*t4)/t3))/(b3*t4 + b4*t4),2)) + 
				   (pow(eposperp,2)*pow(-((t2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
							  (t1*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1))) - 
							(b2*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(((b1*t2 + b2*t2)*(vnegpara - (t4*vnegperp)/t3))/(b3*t4 + b4*t4) + vpospara - (t2*vposperp)/t1))/
							(b1*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)),2))/
				   (pow(dilufit,2)*pow(b2 + (b1*t2)/t1 + ((b1*t2 + b2*t2)*(b4 + (b3*t4)/t3))/(b3*t4 + b4*t4),2)) + 
				   (pow(enegpara,2)*pow(((b1*t2 + b2*t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
							((b3*t4 + b4*t4)*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1)) - 
							(((b2*t1)/b1 + t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(((b1*t2 + b2*t2)*(vnegpara - (t4*vnegperp)/t3))/(b3*t4 + b4*t4) + vpospara - (t2*vposperp)/t1))/
							(((b4*t3)/b3 + t4)*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)),2))/
				   (pow(dilufit,2)*pow(b2 + (b1*t2)/t1 + ((b1*t2 + b2*t2)*(b4 + (b3*t4)/t3))/(b3*t4 + b4*t4),2)) + 
				   (pow(enegperp,2)*pow(-(((b1*t2 + b2*t2)*t4*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4)))/
							  (t3*(b3*t4 + b4*t4)*((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1))) - 
							(b4*((b2*t1)/b1 + t2)*(1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(((b1*t2 + b2*t2)*(vnegpara - (t4*vnegperp)/t3))/(b3*t4 + b4*t4) + vpospara - (t2*vposperp)/t1))/
							(b3*((b4*t3)/b3 + t4)*pow((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1,2)),2))/
				   (pow(dilufit,2)*pow(b2 + (b1*t2)/t1 + ((b1*t2 + b2*t2)*(b4 + (b3*t4)/t3))/(b3*t4 + b4*t4),2)) + 
				   (pow(Edilufit,2)*pow(-1 + t2/t1 - ((b1*t2 + b2*t2)*(1 - t4/t3))/(b3*t4 + b4*t4) + 
							((1 + b2/b1 + ((1 + b4/b3)*((b2*t1)/b1 + t2))/((b4*t3)/b3 + t4))*(((b1*t2 + b2*t2)*(vnegpara - (t4*vnegperp)/t3))/(b3*t4 + b4*t4) + vpospara - (t2*vposperp)/t1))/
							((((b2*t1)/b1 + t2)*(vnegpara + (b4*vnegperp)/b3))/((b4*t3)/b3 + t4) + vpospara + (b2*vposperp)/b1),2))/
				   (pow(dilufit,4)*pow(b2 + (b1*t2)/t1 + ((b1*t2 + b2*t2)*(b4 + (b3*t4)/t3))/(b3*t4 + b4*t4),2))));

    }



  }


  vector<TH1F*> ret;
  ret.push_back(form1);
  ret.push_back(form2);
  ret.push_back(form3);
  return ret;
}



vector<TH1F*> TAsy::ReturnCRS(){
  return crs;


}



vector<Double_t> TAsy::ReturnDilution(){
  vector<Double_t> ret;
  ret.push_back(FITD->GetParameter(0));
  ret.push_back(FITD->GetParError(0));
  ret.push_back(d2);
  ret.push_back(d2err);
  return ret;
}






vector<pair<Double_t, Double_t> > TAsy::GetPols(){
  Topo;
  index;
  ifstream read((inputfiledir+inputfilename+".out").c_str());
  string line;
  //___________________count pols, first pair is beam, second pair is target
  pair<Double_t, Double_t> p0ins(0,0);//beam and target

  vector<pair<Double_t, Double_t> > polinf(4,p0ins);//four pol confs in one bin
  //___________________
  //___________________PERP_POS
  //___________________PARA_POS
  //___________________PERP_NEG
  //___________________PARA_NEG
  read.clear();
  read.seekg(0,ios::beg);
  while(getline(read,line)){
    string::size_type findind = line.find("Bin_index:",0);
    if(findind!=string::npos){
      istringstream iss1(line);
      string a,b;
      Int_t A,B;
      iss1>>a>>A>>b>>B;
      if(A==index && B==Topo){
	for(Int_t subl=0; subl<4; subl++){
	  string conf;
	  Double_t BP,TP;
	  read>>conf>>BP>>TP;
	  polinf[subl].first = fabs(BP);
	  polinf[subl].second = fabs(TP);
	}
      }
    }
  }
  return polinf;
}



void TAsy::GetRawDILU(){
  vector<pair<Double_t, Double_t> > polinf = TAsy::GetPols();
 
  Double_t b1=polinf[0].first;
  Double_t b2=polinf[1].first;
  Double_t b3=polinf[2].first;
  Double_t b4=polinf[3].first;
  Double_t t1=fabs(polinf[0].second);
  Double_t t2=fabs(polinf[1].second);
  Double_t t3=fabs(polinf[2].second);
  Double_t t4=fabs(polinf[3].second);
  Double_t co_a = b2/b1;
  Double_t co_b = b4/b3;
  Double_t co_c = (t2+co_a*t1)/(t4+co_b*t3);
  Double_t co_e = (1+co_a)/(1+co_b);
  Double_t co_m = t2/t1;
  Double_t co_n = t4/t3;
  Double_t co_k = (b2+co_m*b1)/(b4+co_n*b3);
  Double_t co_j = (b2*t2+co_m*b1*t1)/(b4*t4+co_n*b3*t3);


  TH1F* bb = new TH1F();
  bb = (TH1F*)b_MM_BPARA_TPOS_adjusted->Clone();
  bb->Add(b_MM_BPERP_TPOS_adjusted,co_a);
  bb->Add(b_MM_BPARA_TNEG_adjusted,co_c);
  bb->Add(b_MM_BPERP_TNEG_adjusted,co_c*co_b);

  TH1F* cc = new TH1F();
  cc = (TH1F*)c_MM_BPARA_TPOS_adjusted->Clone();
  cc->Add(c_MM_BPERP_TPOS_adjusted,co_a);
  cc->Add(c_MM_BPARA_TNEG_adjusted,co_c);
  cc->Add(c_MM_BPERP_TNEG_adjusted,co_c*co_b);




  for(Int_t ii=1; ii<bb->GetXaxis()->GetNbins(); ii++){
    Double_t err_b = sqrt(bb->GetBinContent(ii));
    Double_t err_c = sqrt(cc->GetBinContent(ii));
    bb->SetBinError(ii, err_b);   
    cc->SetBinError(ii, err_c);
  }
  
  
  
  vector<Double_t> KB1 = bindet->ReturnKBvec("gammaE");
  Int_t KB1size = KB1.size();
  Int_t gammaindex = index/KB1size;

  string mmc_table("/Users/yuqing/work/stage3/kbplot/finals_test/mm_cut_table/range.mmc");
  vector<Double_t> mmc = GetMMC2(mmc_table, gammaindex+1, Topo);
  Double_t MM2min = mmc[0]-mmc[1]*2;
  Double_t MM2max = mmc[0]+mmc[1]*2;

  Double_t dMMB, dMMC;
  Double_t MMB = bb->IntegralAndError(bb->FindBin(MM2min),bb->FindBin(MM2max), dMMB);
  Double_t MMC = cc->IntegralAndError(cc->FindBin(MM2min),cc->FindBin(MM2max), dMMC);
  Double_t alpha = PAR[3];
  Double_t alphaE = PARERROR[3];

  dh = (MMB-alpha*MMC)/MMB;
  dhE = sqrt((pow(alpha,2)*pow(dMMC,2))/pow(MMB,2) + 
		      (pow(alphaE,2)*pow(MMC,2))/pow(MMB,2) + 
		      pow(dMMB,2)*pow(1/MMB - (MMB - alpha*MMC)/pow(MMB,2),2));

  //______________________
  d4 = ((dilu4v[1]+co_a*dilu4v[0]+co_c*(dilu4v[3]+co_b*dilu4v[2])) - alpha*(dilu4v[5]+co_a*dilu4v[4]+co_c*(dilu4v[7]+co_b*dilu4v[6])))/
    (dilu4v[1]+co_a*dilu4v[0]+co_c*(dilu4v[3]+co_b*dilu4v[2]));
  
  d2 = dh;
  d2err = dhE;
}






void TAsy::GetPZDILU(){
  Double_t mml(0.8);
  Double_t mmr(1);
  Double_t alpha = PAR[3];
  Double_t alphaE = PARERROR[3];


  vector<pair<Double_t, Double_t> > polinf = TAsy::GetPols();
  Double_t b1=polinf[0].first;
  Double_t b2=polinf[1].first;
  Double_t b3=polinf[2].first;
  Double_t b4=polinf[3].first;
  Double_t t1=fabs(polinf[0].second);
  Double_t t2=fabs(polinf[1].second);
  Double_t t3=fabs(polinf[2].second);
  Double_t t4=fabs(polinf[3].second);
  Double_t co_f = (t2+t1)/(t4+t3);


  // cout<<co_f<<"   f"<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl<<endl;



  Double_t dMMB1,dMMB2,dMMB3,dMMB4,dMMC1,dMMC2,dMMC3,dMMC4;

  Double_t MMB1 = b_MM_BPERP_TPOS_adjusted->IntegralAndError(b_MM_BPERP_TPOS_adjusted->FindBin(mml),b_MM_BPERP_TPOS_adjusted->FindBin(mmr), dMMB1);
  Double_t MMB2 = b_MM_BPARA_TPOS_adjusted->IntegralAndError(b_MM_BPARA_TPOS_adjusted->FindBin(mml),b_MM_BPARA_TPOS_adjusted->FindBin(mmr), dMMB2);
  Double_t MMB3 = b_MM_BPERP_TNEG_adjusted->IntegralAndError(b_MM_BPERP_TNEG_adjusted->FindBin(mml),b_MM_BPERP_TNEG_adjusted->FindBin(mmr), dMMB3);
  Double_t MMB4 = b_MM_BPARA_TNEG_adjusted->IntegralAndError(b_MM_BPARA_TNEG_adjusted->FindBin(mml),b_MM_BPARA_TNEG_adjusted->FindBin(mmr), dMMB4);

  Double_t MMC1 = c_MM_BPERP_TPOS_adjusted->IntegralAndError(c_MM_BPERP_TPOS_adjusted->FindBin(mml),c_MM_BPERP_TPOS_adjusted->FindBin(mmr), dMMC1);
  Double_t MMC2 = c_MM_BPARA_TPOS_adjusted->IntegralAndError(c_MM_BPARA_TPOS_adjusted->FindBin(mml),c_MM_BPARA_TPOS_adjusted->FindBin(mmr), dMMC2);
  Double_t MMC3 = c_MM_BPERP_TNEG_adjusted->IntegralAndError(c_MM_BPERP_TNEG_adjusted->FindBin(mml),c_MM_BPERP_TNEG_adjusted->FindBin(mmr), dMMC3);
  Double_t MMC4 = c_MM_BPARA_TNEG_adjusted->IntegralAndError(c_MM_BPARA_TNEG_adjusted->FindBin(mml),c_MM_BPARA_TNEG_adjusted->FindBin(mmr), dMMC4);

  Double_t MMC = MMC1+MMC2+MMC3+MMC4;
  Double_t dMMC = sqrt(MMC);

  MMC = MMC*21;
  dMMC = dMMC;

  d = ((MMB1+MMB2) + co_f*(MMB3+MMB4) - MMC*(2+2*co_f)/4) / ((MMB1+MMB2) + co_f*(MMB3+MMB4));

  derr = (pow(-2 - 2*co_f,2)*pow(dMMC,2))/
    (16.*pow(MMB1 + MMB2 + co_f*(MMB3 + MMB4),2)) + 
    pow(dMMB1,2)*pow(1/(MMB1 + MMB2 + co_f*(MMB3 + MMB4)) - 
			 (MMB1 + MMB2 + co_f*(MMB3 + MMB4) - ((2 + 2*co_f)*MMC)/4.)/
			 pow(MMB1 + MMB2 + co_f*(MMB3 + MMB4),2),2) + 
    pow(dMMB2,2)*pow(1/(MMB1 + MMB2 + co_f*(MMB3 + MMB4)) - 
			 (MMB1 + MMB2 + co_f*(MMB3 + MMB4) - ((2 + 2*co_f)*MMC)/4.)/
			 pow(MMB1 + MMB2 + co_f*(MMB3 + MMB4),2),2) + 
    pow(dMMB3,2)*pow(co_f/(MMB1 + MMB2 + co_f*(MMB3 + MMB4)) - 
			 (co_f*(MMB1 + MMB2 + co_f*(MMB3 + MMB4) - ((2 + 2*co_f)*MMC)/4.))/
			 pow(MMB1 + MMB2 + co_f*(MMB3 + MMB4),2),2) + 
    pow(dMMB4,2)*pow(co_f/(MMB1 + MMB2 + co_f*(MMB3 + MMB4)) - 
			 (co_f*(MMB1 + MMB2 + co_f*(MMB3 + MMB4) - ((2 + 2*co_f)*MMC)/4.))/
			 pow(MMB1 + MMB2 + co_f*(MMB3 + MMB4),2),2);





  // Double_t vpz = ((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*co_f) / (((MMB1+MMB2) + (MMB3+MMB4) - MMC*(2+2*co_f)/4) * (t1+t2+t3+t4));

  Double_t vpz = ((MMB1+MMB2 - (MMB3+MMB4))/(MMB1+MMB2 + co_f*(MMB3+MMB4))) * (1/d) * ((2+2*co_f)/(t1+t2+t3+t4));

  Double_t epz = pow(dMMB3,2)*pow((-0.8333333333333333*(2 + 2*co_f)*
				   (MMB1 + MMB2 - MMB3 - MMB4))/pow(MMB1 + MMB2 + MMB3 + MMB4,2)\
				  - (0.8333333333333333*(2 + 2*co_f))/(MMB1 + MMB2 + MMB3 + MMB4),2)\
    + pow(dMMB4,2)*pow((-0.8333333333333333*(2 + 2*co_f)*
			(MMB1 + MMB2 - MMB3 - MMB4))/pow(MMB1 + MMB2 + MMB3 + MMB4,2)\
		       - (0.8333333333333333*(2 + 2*co_f))/(MMB1 + MMB2 + MMB3 + MMB4),2)\
    + pow(dMMB1,2)*pow((-0.8333333333333333*(2 + 2*co_f)*
			(MMB1 + MMB2 - MMB3 - MMB4))/pow(MMB1 + MMB2 + MMB3 + MMB4,2)\
		       + (0.8333333333333333*(2 + 2*co_f))/(MMB1 + MMB2 + MMB3 + MMB4),2)\
    + pow(dMMB2,2)*pow((-0.8333333333333333*(2 + 2*co_f)*
			(MMB1 + MMB2 - MMB3 - MMB4))/pow(MMB1 + MMB2 + MMB3 + MMB4,2)\
		       + (0.8333333333333333*(2 + 2*co_f))/(MMB1 + MMB2 + MMB3 + MMB4),2);

  PZ->SetPoint(0, 0, vpz);
  PZ->SetPointError(0, 0, epz);

  cout<<"PZ:     "<<vpz<<"PZerr:     "<<epz<<endl<<endl<<endl<<endl;


}



// CForm[D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vposperp]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vposperp]*
//       eposperp*eposperp
//       +
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vpospara]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vpospara]*
//       epospara*epospara
//       +
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vnegperp]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vnegperp]*
//       enegperp*enegperp
//       +
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vnegpara]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vnegpara]*
//       enegpara*enegpara
//       +

//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCposperp]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCposperp]*
//       eCposperp*eCposperp
//       +
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCpospara]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCpospara]*
//       eCpospara*eCpospara
//       +
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCnegperp]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCnegperp]*
//       eCnegperp*eCnegperp
//       +
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCnegpara]*
//       D[(vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp) - (vCpospara + coa*vCposperp + coc*( vCnegpara + cob*vCnegperp)))/
// 	( vpospara + coa*vposperp + coc*( vnegpara + cob*vnegperp)), vCnegpara]*
//       eCnegpara*eCnegpara
//       ]



// CForm[D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB1]*
//       D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB1]*
//       dMMB1*dMMB1
//       +D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB2]*
//       D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB2]*
//       dMMB2*dMMB2
//       +D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB3]*
//       D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB3]*
//       dMMB3*dMMB3
//       +D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB4]*
//       D[(((MMB1+MMB2) - (MMB3+MMB4)) * (2+2*cof)) / (((MMB1+MMB2) + (MMB3+MMB4)) * 3)/0.4, MMB4]*
//       dMMB4*dMMB4]


// CForm[D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB1]*
//       D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB1]*
//       dMMB1*dMMB1
//       +D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB2]*
//       D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB2]*
//       dMMB2*dMMB2
//       +D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB3]*
//       D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB3]*
//       dMMB3*dMMB3
//       +D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB4]*
//       D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMB4]*
//       dMMB4*dMMB4
//       +D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMC]*
//       D[((MMB1+MMB2) + cof*(MMB3+MMB4) - MMC*(2+2*cof)/4) / ((MMB1+MMB2) + cof*(MMB3+MMB4)), MMC]*
//       dMMC*dMMC]

vector<Double_t> TAsy::GetCMMs(){
  return normcheck;
}



Double_t TAsy::GetEntries(){
  return entries;
}


vector<TH1F*> TAsy::GetResultElements(){
  // TObjArray* ret = new TObjArray(20);
  vector<TH1F*> RET;
  RET=alge;
  // RET.push_back(PZ);

  // RET.push_back(alge[1]);
  // RET.push_back(alge[2]);


  // ret->AddAt(asy_perp_front, 1);
  // ret->AddAt(asy_perp_back,  2);
  // ret->AddAt(asy_para_front, 3);
  // ret->AddAt(asy_para_back,  4);
  // ret->AddAt(asy_perp,5);
  // ret->AddAt(asy_para,6);
  // ret->AddAt(asy_combine,7);
  // ret->AddAt(betastats[0],8);
  // ret->AddAt(betastats[1],9);
  // ret->AddAt(asy_carbon,10);
  // ret->AddAt(posNneg,11);
  // ret->AddAt(alge[0],12);//pszpsc
  // ret->AddAt(alge[1],13);//pz
  // ret->AddAt(alge[2],14);//isic

  // // ret->AddAt(asy_carbon,8);
  return RET;
}
 




// Double_t BinCenter = PERPPOS->GetBinCenter(i);
// Double_t BinWidth = PERPPOS->GetXaxis()->GetBinWidth(i)/2;
// form1->SetPoint(ii, BinCenter, (v_posperp - v_pospara - v_negperp + v_negpara)/(v_posperp + v_pospara + v_negperp + v_negpara));
// form1->SetPointError(ii, BinWidth,  sqrt(pow(e_pospara,2)*pow(-((v_posperp - v_pospara - v_negperp + v_negpara)/pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) - 
// 								  1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_negperp,2)*pow(-((v_posperp - v_pospara - v_negperp + v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) - 1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_posperp,2)*pow(-((v_posperp - v_pospara - v_negperp + v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) + 1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_negpara,2)*pow(-((v_posperp - v_pospara - v_negperp + v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) + 1/(v_posperp + v_pospara + v_negperp + v_negpara),2)));



// form2->SetPoint(ii, BinCenter, (v_posperp + v_pospara - v_negperp - v_negpara)/(v_posperp + v_pospara + v_negperp + v_negpara));
// form2->SetPointError(ii, BinWidth,  sqrt(pow(e_negperp,2)*pow(-((v_posperp + v_pospara - v_negperp - v_negpara)/pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) - 
// 								  1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_negpara,2)*pow(-((v_posperp + v_pospara - v_negperp - v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) - 1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_posperp,2)*pow(-((v_posperp + v_pospara - v_negperp - v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) + 1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_pospara,2)*pow(-((v_posperp + v_pospara - v_negperp - v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) + 1/(v_posperp + v_pospara + v_negperp + v_negpara),2)));



// form3->SetPoint(ii, BinCenter, (v_posperp - v_pospara + v_negperp - v_negpara)/(v_posperp + v_pospara + v_negperp + v_negpara));
// form3->SetPointError(ii, BinWidth,  sqrt(pow(e_pospara,2)*pow(-((v_posperp - v_pospara + v_negperp - v_negpara)/pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) - 
// 								  1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_negpara,2)*pow(-((v_posperp - v_pospara + v_negperp - v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) - 1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_posperp,2)*pow(-((v_posperp - v_pospara + v_negperp - v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) + 1/(v_posperp + v_pospara + v_negperp + v_negpara),2) + 
// 					     pow(e_negperp,2)*pow(-((v_posperp - v_pospara + v_negperp - v_negpara)/
// 								    pow(v_posperp + v_pospara + v_negperp + v_negpara,2)) + 1/(v_posperp + v_pospara + v_negperp + v_negpara),2)));



vector<Double_t> TAsy::PreGaussFit(TH1* b, TH1* c, Double_t pre){
  TH1F* bb = (TH1F*)b->Clone("bb");
  TH1F* cc = (TH1F*)c->Clone("cc");
  cc->Scale(pre);
  bb->Add(cc,-1);

  if(Topo == 1){
    pre_peak_fit = new TF1("pre_peak","gaus",0.85,0.96);
  }
  else if(Topo == 2){
    pre_peak_fit = new TF1("pre_peak","gaus",0.82,0.97);
  }
  else if(Topo == 3){
    pre_peak_fit = new TF1("pre_peak","gaus",0.02,0.06);
  } 
  else if(Topo == 4){
    pre_peak_fit = new TF1("pre_peak","gaus",0.02,0.06);
  }

  bb->Fit(pre_peak_fit,"R");

  vector<Double_t> ret;
  ret.push_back(pre_peak_fit->GetParameter(0));
  ret.push_back(pre_peak_fit->GetParameter(1));
  ret.push_back(pre_peak_fit->GetParameter(2));

  pre_peak_hist = (TH1F*)bb->Clone();
  return ret;
}


void TAsy::LinearDF(){
  vector<pair<Double_t, Double_t> > polinf = TAsy::GetPols();
  Double_t b1=polinf[0].first;
  Double_t b2=polinf[1].first;
  Double_t b3=polinf[2].first;
  Double_t b4=polinf[3].first;
  Double_t t1=fabs(polinf[0].second);
  Double_t t2=fabs(polinf[1].second);
  Double_t t3=fabs(polinf[2].second);
  Double_t t4=fabs(polinf[3].second);
  Double_t co_a = b2/b1;
  Double_t co_b = b4/b3;
  Double_t co_c = (t2+co_a*t1)/(t4+co_b*t3);
  Double_t co_e = (1+co_a)/(1+co_b);
  Double_t co_m = t2/t1;
  Double_t co_n = t4/t3;
  Double_t co_k = (b2+co_m*b1)/(b4+co_n*b3);
  Double_t co_j = (b2*t2+co_m*b1*t1)/(b4*t4+co_n*b3*t3);
  Double_t C1a = 1-co_m-co_k*(1-co_n);
  Double_t C2a = b2*t2+b1*t1*co_m + co_k*(b4*t4+b3*t3*co_n);
  Double_t C1b = 1-co_m+co_j*(1-co_n);
  Double_t C2b = b2+b1*co_m+co_j*(b4+b3*co_n);
  Double_t C1c = t2+co_a*t1+(t4+co_b*t3)*co_e;
  Double_t C2c = b2*(t2-t1) + b4*(t4-t3)*co_e;
  Double_t C1d = 1+co_a+co_c*(1+co_b);
  Double_t C2d = b2*(t2-t1-t4+t3);


  // b = (TH1F*)b_MM_BPERP_TPOS_adjusted->Clone();
  // b->Add(b_MM_BPARA_TPOS_adjusted,1);
  // b->Add(b_MM_BPERP_TNEG_adjusted,1);
  // b->Add(b_MM_BPARA_TNEG_adjusted,1);

  //   Double_t pre1 = ( v_pospara + co_a*v_posperp + co_c*( v_negpara + co_b*v_negperp));


  //   Double_t fin1;
  //   Double_t fin2;
  //   Double_t fin3;

  //   fin1 = (pre1*C1d - C1a)/C2a;
  //   if(index/18==3 || index/18==0)  fin2= (pre2*C1d-0.1)/C1c;
  //   else fin2= (pre2*C1d)/C1c;
  //   fin3 = (pre3*C1d - C1b)/C2b;

}

vector<Double_t> TAsy::SideBandFit(TH1F* b,TH1F* c, Double_t v1, Double_t v2, Double_t v3, Double_t v4){
  vector<Double_t> ret;
  v1 = fabs(v1);
  v2 = fabs(v2);
  v3 = fabs(v3);
  TH1F* bb = (TH1F*)b->Clone("bb");
  TH1F* cc = (TH1F*)c->Clone("cc");

  for(Int_t ii=1; ii<bb->GetXaxis()->GetNbins(); ii++){
    Double_t err_b = sqrt(bb->GetBinContent(ii));
    Double_t err_c = sqrt(cc->GetBinContent(ii));
    // Double_t err_b = bb->GetBinError(ii);
    // Double_t err_c = cc->GetBinError(ii);
    Double_t pre_sf = 7.2;
    Double_t bin_err = sqrt(err_b*err_b + pre_sf*pre_sf*err_c*err_c);
    bb->SetBinError(ii, bin_err);   
    cc->SetBinError(ii, 0);
  }

  TF1* sfit;
  Side_Band_Class* sbc = new Side_Band_Class(c); 
  
  if(Topo == 1){
    sfit = new TF1("sideband",sbc,0,v2-1*v3,2,"Side_Band_Class");
  }
  else if(Topo == 2){
    sfit = new TF1("sideband",sbc,0,v2-1*v3,2,"Side_Band_Class");
  }
  else if(Topo == 3){
    sfit = new TF1("sideband",sbc,-0.2,v2-1*v3,2,"Side_Band_Class");
  } 
  else if(Topo == 4){
    sfit = new TF1("sideband",sbc,-0.2,v2-1*v3,2,"Side_Band_Class");
  }

  bb->Fit(sfit,"R");

  ret.push_back(sfit->GetParameter(0));
  ret.push_back(sfit->GetParError(0));


  return ret;
}



TAsy* TAsy::SCALE(){
  Double_t limit = 0.001;

  TH1F* b = new TH1F();
  b = (TH1F*)b_MM_BPERP_TPOS_adjusted->Clone();
  b->Add(b_MM_BPARA_TPOS_adjusted,1);
  b->Add(b_MM_BPERP_TNEG_adjusted,1);
  b->Add(b_MM_BPARA_TNEG_adjusted,1);
  TH1F* c = new TH1F();
  c = (TH1F*)c_MM_BPERP_TPOS_adjusted->Clone();
  c->Add(c_MM_BPARA_TPOS_adjusted,1);
  c->Add(c_MM_BPERP_TNEG_adjusted,1);
  c->Add(c_MM_BPARA_TNEG_adjusted,1);

  b->Rebin(100);
  c->Rebin(100);
  // BACK->Rebin(100);


  b_sum = (TH1F*)b->Clone();
  // c_sum = (TH1F*)c->Clone();
  // c_sum_scaled = (TH1F*)c->Clone();
  c_sum = (TH1F*)c->Clone();
  c_sum_scaled = (TH1F*)c->Clone();

  // MyFunc_BackgroundFit* fptr = new MyFunc_BackgroundFit(c); 
  MyFunc_BackgroundFit* fptr = new MyFunc_BackgroundFit(c); 

  for(Int_t ii=1; ii<b->GetXaxis()->GetNbins(); ii++){
    Double_t err_b = b->GetBinError(ii);
    Double_t err_c = c->GetBinError(ii);
    Double_t pre_sf = 20;
    Double_t bin_err = sqrt(err_b*err_b + pre_sf*pre_sf*err_c*err_c);
    b->SetBinError(ii, bin_err);   
    c->SetBinError(ii, 0);
  }


  Double_t range_fronta,range_frontb, range_back;
  TF1* total;
  if(Topo==1){
    range_fronta = 0.5;
    range_frontb = 0.85;
    range_back = 1.;
    total = new TF1("total",fptr,0.5,1,4,"MyFunc_BackgroundFit");
  }
  if(Topo==2){
    range_fronta = 0.5;
    range_frontb = 0.85;
    range_back = 1.;
    total = new TF1("total",fptr,0.5,1.,4,"MyFunc_BackgroundFit");
  }
  if(Topo==3){
    range_fronta = -0.01;
    range_back = 0.05;
    total = new TF1("total",fptr,-0.1,0.2,4,"MyFunc_BackgroundFit");
  }
  if(Topo==4){
    range_fronta = -0.01;
    range_back = 0.05;
    total = new TF1("total",fptr,-0.1,0.2,4,"MyFunc_BackgroundFit");
  }
  
  Double_t b_front        = b->Integral(b->GetXaxis()->FindBin(range_fronta),b->GetXaxis()->FindBin(range_frontb));
  Double_t c_front        = c->Integral(c->GetXaxis()->FindBin(range_fronta),c->GetXaxis()->FindBin(range_frontb));
  Double_t b_back         = b->Integral(b->GetXaxis()->FindBin(range_back),b->GetXaxis()->GetNbins());
  Double_t c_back         = c->Integral(c->GetXaxis()->FindBin(range_back),c->GetXaxis()->GetNbins());
  Double_t ratio_front    = b_front/c_front;
  Double_t ratio_back     = b_back/c_back;
  Double_t ratio_small    = (ratio_front<ratio_back)?ratio_front:ratio_back;
  Double_t ratio_large    = (ratio_front>ratio_back)?ratio_front:ratio_back;
  Double_t ratio_mid      = ratio_front;
  if(Topo==3) ratio_mid = ratio_back;
  Double_t _low        = ratio_mid * (1-limit);
  Double_t _upp      = ratio_mid * (1+limit);

  vector<Double_t> GetPrimilinaryGauss = TAsy::PreGaussFit(b,c,ratio_mid);
  total->SetParameter(0,GetPrimilinaryGauss[0]);
  total->SetParameter(1,GetPrimilinaryGauss[1]);
  total->SetParameter(2,GetPrimilinaryGauss[2]);
  total->SetParameter(3,ratio_front);
  // total->SetParLimits(3, _low, _upp);
  // total->FixParameter(3, ratio_mid);

  b->Fit(total,""); 
  TOT = total;

  vector<Double_t> GetFinalAlpha = TAsy::SideBandFit(b,c,
						     total->GetParameter(0),
						     total->GetParameter(1),
						     total->GetParameter(2),
						     total->GetParameter(3));

  //___________set the global object variables
  c_sum_scaled->Scale(GetFinalAlpha[0]);

  for(Int_t fit_pars=0;fit_pars<3;fit_pars++){
    PAR[fit_pars] = total->GetParameter(fit_pars);
    PARERROR[fit_pars] = total->GetParError(fit_pars);
  }
  PAR[3] = GetFinalAlpha[0];
  PARERROR[3] = GetFinalAlpha[1];

  return this;
}






vector<Double_t> TAsy::ReturnAlpha(){
  vector<Double_t> ret;
  ret.push_back(PAR[3]);
  ret.push_back(PARERROR[3]);

  return ret;
}




TAsy* TAsy::WriteTable(string add){
  ofstream w(add.c_str(), ios::app);
  w<<"Topology:    "<<Topo<<"    Index:  "<< index << endl;
  // w<<"DF:    "<<DF<<endl;
  w<<"SF:    "<<PAR[3]<<"   "<<PARERROR[3]<<endl;
  w<<"mean:    "<<PAR[1]<<endl;
  w<<"sigma:    "<<PAR[2]<<endl;
  w<<endl;
  return this;
}





TAsy* TAsy::Monitor(){
  gStyle->SetOptTitle(0);
  gStyle->SetPadTickX(1);
  gStyle->SetPadTickY(1);
  string draw_option = "E1";
  string draw_option_same = "sameE1";
  TCanvas *wr = new TCanvas("wr","wr",1500,1000);
  gStyle->SetOptStat(0);
  wr->SetGrid();
  wr->SetTicks(); 
  wr->SetFillColor(0); 
  wr->SetFrameFillColor(0);
  wr->Divide(3,3);
  /////


  TH1F* b_sum_draw = new TH1F();
  b_sum_draw = (TH1F*)b_sum->Clone("b_sum_draw");
  TH1F* c_sum_draw = new TH1F();
  // c_sum_draw = (TH1F*)c_sum->Clone("c_sum_draw");
  c_sum_draw = (TH1F*)c_sum->Clone("c_sum_draw");

  Double_t legend_size = 0.04;
  wr->cd(1);
  b_sum_draw->GetXaxis()->SetTitle("Missing Mass Square   (GeV^{2})");
  b_sum_draw->GetXaxis()->SetLabelSize(0.05);
  b_sum_draw->GetYaxis()->SetLabelSize(0.05);
  b_sum_draw->GetXaxis()->SetTitleSize(0.05);
  b_sum_draw->GetYaxis()->SetTitleSize(0.05);
  b_sum_draw->SetLineColor(2);
  b_sum_draw->SetFillColor(2); 
  c_sum->SetLineColor(9);
  c_sum->SetFillColor(9);
  pre_peak_hist->SetLineColor(1);
  b_sum_draw->Draw("");
  c_sum->Draw("same");  
  //pre_peak_hist->Draw("same");
  TLegend* leg1 = new TLegend(0.5,0.6,0.88,0.8);
  leg1-> SetBorderSize(0);
  leg1-> SetShadowColor(0);
  leg1->SetTextSize(legend_size);
  leg1->AddEntry(b_sum_draw,"Butanol","f");
  leg1->AddEntry(c_sum,"Carbon","f");
  //leg1->AddEntry(pre_peak_hist,"Pre-Scaled C^{12}","f");
  leg1->Draw();


  wr->cd(2);
  pre_peak_hist->Draw("");
  pre_peak_fit->Draw("same");
  pre_peak_hist->SetTitle("Missing Mass Square for Butanol and carbon target");
  pre_peak_hist->GetXaxis()->SetTitle("Missing Mass Square   (GeV^{2})");
  // pre_peak_hist->GetYaxis()->SetTitle("Entries");
  pre_peak_hist->GetXaxis()->SetLabelSize(0.05);
  pre_peak_hist->GetYaxis()->SetLabelSize(0.05);
  pre_peak_hist->GetXaxis()->SetTitleSize(0.05);
  pre_peak_hist->GetYaxis()->SetTitleSize(0.05);
  pre_peak_hist->SetLineWidth(3);
  pre_peak_hist->SetFillColor(31);
  pre_peak_hist->SetLineColor(31);
  TLegend* leg2 = new TLegend(0.5,0.6,0.88,0.8);
  leg2->SetBorderSize(0);
  leg2->SetShadowColor(0);
  leg2->SetTextSize(legend_size);
  leg2->AddEntry(pre_peak_hist,"Pre-free signal","lep");
  leg2->AddEntry(pre_peak_fit,"Gaussian Fit","LEP"); 
  leg2->Draw();



  wr->cd(3);
  b_sum_draw->GetXaxis()->SetTitle("Missing Mass Square   (GeV^{2})");
  b_sum_draw->GetXaxis()->SetLabelSize(0.05);
  b_sum_draw->GetYaxis()->SetLabelSize(0.05);
  b_sum_draw->GetXaxis()->SetTitleSize(0.05);
  b_sum_draw->GetYaxis()->SetTitleSize(0.05);
  b_sum_draw->SetFillColor(2);
  b_sum_draw->SetLineColor(2);
  TOT->SetLineWidth(2);
  b_sum_draw->Draw("");
  TOT->Draw("same");
  TLegend* leg3 = new TLegend(0.5,0.6,0.88,0.8);
  leg3->SetBorderSize(0);
  leg3->SetShadowColor(0);
  leg3->SetTextSize(legend_size);
  leg3->AddEntry(b_sum_draw,"Butanol","f");
  leg3->AddEntry(TOT,"Total Fit","LEP"); 
  leg3->Draw();

  
  vector<Double_t> KB1 = bindet->ReturnKBvec("gammaE");
  Int_t KB1size = KB1.size();
  Int_t gammaindex = index/KB1size;

  string mmc_table("/Users/yuqing/work/stage3/kbplot/finals_test/mm_cut_table/range.mmc");
  vector<Double_t> mmc = GetMMC2(mmc_table, gammaindex+1, Topo);
  Double_t MM2min = mmc[0]-mmc[1]*2;
  Double_t MM2max = mmc[0]+mmc[1]*2;

  Double_t dMMB, dMMC;
  Double_t MMB = b_sum_draw->IntegralAndError(b_sum_draw->FindBin(MM2min),b_sum_draw->FindBin(MM2max), dMMB);
  Double_t MMC = c_sum_scaled->IntegralAndError(c_sum_scaled->FindBin(MM2min),c_sum_scaled->FindBin(MM2max), dMMC);
  Double_t alpha = PAR[3];
  Double_t alphaE = PARERROR[3];

  Double_t dh2 = (MMB-MMC)/MMB;
  Double_t dh2E = sqrt((pow(alpha,2)*pow(dMMC,2))/pow(MMB,2) + 
		       (pow(alphaE,2)*pow(MMC,2))/pow(MMB,2) + 
		       pow(dMMB,2)*pow(1/MMB - (MMB - alpha*MMC)/pow(MMB,2),2));
  //_______________

  wr->cd(4);
  b_sum_draw->Draw("B");  
  c_sum_scaled->Draw("same");
  b_sum_draw->GetXaxis()->SetTitle("Missing Mass Square   (GeV^{2})");
  b_sum_draw->GetXaxis()->SetLabelSize(0.05);
  b_sum_draw->GetYaxis()->SetLabelSize(0.05);
  b_sum_draw->GetXaxis()->SetTitleSize(0.05);
  b_sum_draw->GetYaxis()->SetTitleSize(0.05);
  b_sum_draw->SetLineColor(2);
  b_sum_draw->SetFillColor(2);
  c_sum_scaled->SetLineColor(9);
  c_sum_scaled->SetFillColor(9);
  TLegend* leg4 = new TLegend(0.5,0.6,0.88,0.8);
  leg4->SetBorderSize(0);
  leg4->SetShadowColor(0);
  leg4->SetTextSize(legend_size);
  leg4->AddEntry(b_sum_draw,"Butanol","f");
  leg4->AddEntry(c_sum_scaled,"Scaled Carbon","f"); 
  leg4->Draw();
 

  ostringstream dh2w;
  TPaveText *ptw = new TPaveText(0.02,0.8,0.4,0.95,"NDC");
  ptw->SetTextAlign(11);
  ptw->SetTextSize(0.05);  
  dh2w<<"Dilu(MM):  "<<dh<<"#pm"<<dhE;
  ptw->AddText(dh2w.str().c_str());
  dh2w.str("");
  dh2w<<"Dilu(FIT):  "<<dilufit<<"#pm"<<Edilufit;
  ptw->AddText(dh2w.str().c_str());
  ptw->Draw(); 


  
  wr->cd(5);
  TH1F* FreeProton = new TH1F();
  FreeProton = (TH1F*)b_sum_draw->Clone("FreeProton");
  FreeProton->Add(c_sum_scaled,-1);
  FreeProton->Draw("B");  
  pre_peak_fit->Draw("same");
  FreeProton->GetXaxis()->SetTitle("Missing Mass Square   (GeV^{2})");
  FreeProton->GetXaxis()->SetLabelSize(0.05);
  FreeProton->GetYaxis()->SetLabelSize(0.05);
  FreeProton->GetXaxis()->SetTitleSize(0.05);
  FreeProton->GetYaxis()->SetTitleSize(0.05);
  FreeProton->SetLineColor(8);
  FreeProton->SetFillColor(8);
  TLegend* leg5 = new TLegend(0.5,0.6,0.88,0.8);
  leg5->SetBorderSize(0);
  leg5->SetShadowColor(0);
  leg5->SetTextSize(legend_size);
  leg5->AddEntry(FreeProton,"Free Proton","f");
  leg5->AddEntry(pre_peak_fit,"Gauss Fit","LEP"); 
  leg5->Draw();



  
  wr->cd(6);
  wr->SetGrid();
  alge[3]->Draw("AP");
  // FITD->Draw("same");
  alge[3]->SetMarkerColor(1);
  alge[3]->SetMarkerSize(1);
  alge[3]->SetMarkerStyle(21);
  alge[3]->SetLineColor(1);
  alge[3]->SetLineWidth(1);
  alge[3]->SetLineStyle(1);
  alge[3]->GetXaxis()->SetRangeUser(0.,3.1415926*2);
  alge[3]->GetYaxis()->SetRangeUser(-0,1);
  alge[3]->GetXaxis()->SetLabelSize(0.05);
  alge[3]->GetYaxis()->SetLabelSize(0.05);
  alge[3]->GetXaxis()->SetTitleSize(0.05);
  alge[3]->GetYaxis()->SetTitleSize(0.05);
  alge[3]->SetTitle("");
  alge[3]->GetXaxis()->SetTitle("#phi   (Rad)");
  alge[3]->GetYaxis()->SetTitle("Dilution Factor");
  alge[3]->GetXaxis()->CenterTitle(1);
  alge[3]->GetYaxis()->CenterTitle(1);
  TLine *line6 = new TLine();
  line6->SetLineColor(2);
  line6->SetLineStyle(2);
  line6->SetLineWidth(3);
  Double_t upperE = FITD->GetParameter(0) + FITD->GetParError(0);
  Double_t lowerE = FITD->GetParameter(0) - FITD->GetParError(0);
  line6->DrawLine(0,upperE,6.28,upperE);
  line6->DrawLine(0,lowerE,6.28,lowerE);
  

  wr->cd(7);

  TH1F* b_sum7 = new TH1F();
  b_sum7 = (TH1F*)b_sum->Clone("b_sum7");
  TH1F* c_sum7 = new TH1F();
  c_sum7 = (TH1F*)c_sum->Clone("c_sum7");

  b_sum7->Divide(c_sum7);
  b_sum7->Draw("");
  b_sum7->GetXaxis()->SetTitle("Missing Mass Square   (GeV^{2})");
  b_sum7->GetXaxis()->SetLabelSize(0.05);
  b_sum7->GetYaxis()->SetLabelSize(0.05);
  b_sum7->GetXaxis()->SetTitleSize(0.05);
  b_sum7->GetYaxis()->SetTitleSize(0.05);
  b_sum7->SetLineColor(2);
  b_sum7->SetFillColor(2);



  wr->cd(8);
  TPaveText *pt1 = new TPaveText(0.01,0.01,0.49,0.99);
  pt1-> SetBorderSize(0);
  pt1-> SetShadowColor(0);
  pt1->SetTextColor(2);
  pt1->SetTextAlign(11);
  pt1->SetTextSize(0.06);
  ostringstream STR;
  /////
  STR<<"Topology:  "<<Topo<<"  ("<<TAsy::topo_show()<<")";
  pt1->AddText(0.05,0.9,STR.str().c_str());
  /////
  STR.str("");
  pt1->AddText(0.05,0.75,"Fit Function:")->SetTextSize(legend_size);
  STR.str("");
  pt1->AddText(0.05,0.7,"MM_{B} = SF #times MM_{C} + Gaus")->SetTextSize(legend_size);
  /////
  STR.str("");

  /////
  STR<<"Chi2/ndf:   " << Form("%5.2f", TOT->GetChisquare()/TOT->GetNDF());
  pt1->AddText(0.05,0.55,STR.str().c_str());
  /////
  STR.str("");
  STR<<"Scale Factor: "<<Form("%5.2f", PAR[3])<<" #pm "<<Form("%5.2f", PARERROR[3]);
  pt1->AddText(0.05,0.4,STR.str().c_str())->SetTextColor(2);
  /////
  STR.str("");
  STR<<"Dilution Factor: "<<Form("%5.2f", FITD->GetParameter(0))<<" #pm "<<Form("%5.2f", FITD->GetParError(0));
  pt1->AddText(0.05,0.25,STR.str().c_str())->SetTextColor(2);
  /////
  Int_t b_count = b_sum_draw->Integral(0,b_sum_draw->GetNbinsX());
  Int_t c_count = c_sum_draw->Integral(0,c_sum_draw->GetNbinsX());
  STR.str("");
  STR<<"B: "<<Form("%5d", b_count)<<",  C: "<<Form("%5d", c_count);
  pt1->AddText(0.05,0.12,STR.str().c_str());
  /////
  // STR.str("");
  // STR<<"Entries_Carnon:  "<<Form("%5d", c_count);
  // pt1->AddText(0.05,0.08,STR.str().c_str());
  pt1->Draw();


  // TPaveText *pt2 = new TPaveText(0.51,0.01,0.99,0.99);  pt1-> SetBorderSize(1);
  // pt2-> SetBorderSize(0);
  // pt2-> SetShadowColor(0);
  // pt2->SetTextColor(2);
  // pt2->SetTextAlign(11);
  // pt2->SetTextSize(legend_size);
  // /////
  // STR.str("");
  // STR<<"E_{#gamma}:  "<<Range_1;
  // pt2->AddText(0.05,0.9,STR.str().c_str());
  // /////
  // STR.str("");
  // STR<<"IM(pro-pip):    "<<Range_2;
  // pt2->AddText(0.05,0.74,STR.str().c_str());
  // /////
  // STR.str("");
  // STR<<"IM(pip-pim):    "<<Range_3;
  // pt2->AddText(0.05,0.58,STR.str().c_str());
  // /////
  // STR.str("");
  // STR<<"cos(#theta_{CM}):    "<<Range_4;
  // pt2->AddText(0.05,0.42,STR.str().c_str());
  // /////
  // STR.str("");
  // STR<<"#theta:    "<<Range_5;
  // pt2->AddText(0.05,0.26,STR.str().c_str());
  // /////
  // STR.str("");
  // STR<<"#phi:    "<<Range_6;
  // pt2->AddText(0.05,0.1,STR.str().c_str());
  // pt2->Draw();

  ostringstream convert;
  convert<<inputfilename<<+"/ScaleMonitor/Bin_"<<index<<"_Topo_"<<Topo<<".png";
  wr->Print(convert.str().c_str());
  delete wr;


  // TCanvas *m1 = new TCanvas("m1","m1",1500,750);
  // m1->SetGrid();
  // m1->SetTicks(); 
  // m1->SetFillColor(0); 
  // m1->SetFrameFillColor(0);
  // b_sum_draw->Draw("");
  // b_sum_draw->SetTitle("Missing Mass Square for Butanol target");
  // b_sum_draw->GetXaxis()->SetTitle("Missing Mass Square(GeV^{2})");
  // b_sum_draw->GetYaxis()->SetTitle("");
  // b_sum_draw->SetLineColor(2);
  // b_sum_draw->SetFillColor(2);
  // b_sum_draw->GetXaxis()->SetLabelSize(0.045);
  // b_sum_draw->GetYaxis()->SetLabelSize(0.045);
  // b_sum_draw->GetXaxis()->SetTitleSize(0.045);
  // b_sum_draw->GetYaxis()->SetTitleSize(0.045);

  // TCanvas *m2 = new TCanvas("m2","m2",1500,750);
  // m2->SetGrid();
  // m2->SetTicks(); 
  // m2->SetFillColor(0); 
  // m2->SetFrameFillColor(0);
  // B3->Draw("");
  // C3->Draw("same");
  // B3->SetTitle("Missing Mass Square for Butanol and Scaled Carbon");
  // B3->GetXaxis()->SetTitle("Missing Mass Square(GeV^{2})");
  // B3->GetYaxis()->SetTitle("");
  // B3->GetXaxis()->SetLabelSize(0.045);
  // B3->GetYaxis()->SetLabelSize(0.045);
  // B3->GetXaxis()->SetTitleSize(0.045);
  // B3->GetYaxis()->SetTitleSize(0.045);


  // TCanvas *m3 = new TCanvas("m3","m3",1500,750);
  // m3->SetGrid();
  // m3->SetTicks(); 
  // m3->SetFillColor(0); 
  // m3->SetFrameFillColor(0);
  // FreeProton1->Draw("");  
  // fpf->Draw("samehe1");
  // FreeProton1->SetTitle("Missing Mass Square for Free proton");
  // FreeProton1->GetXaxis()->SetTitle("Missing Mass Square(GeV^{2})");
  // FreeProton1->GetYaxis()->SetTitle("");
  // FreeProton1->GetXaxis()->SetLabelSize(0.045);
  // FreeProton1->GetYaxis()->SetLabelSize(0.045);
  // FreeProton1->GetXaxis()->SetTitleSize(0.045);
  // FreeProton1->GetYaxis()->SetTitleSize(0.045);

  return this;
}



string TAsy::topo_show(){
  string ret;
  if(Topo == 1) ret="All detected";
  else if(Topo == 2) ret="Proton missed";
  else if(Topo == 3) ret="#pi+ missed";
  else if(Topo == 4) ret="#pi- missed";
  return ret;
}
